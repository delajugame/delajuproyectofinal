#ifndef PERSONAJES_H
#define PERSONAJES_H
#include "librerias.h"
#include"items.h"
class Personajes: public QObject, public QGraphicsItem//hipolito
{
      Q_OBJECT
public:
    //Propiedades del sprite

    Personajes(int Jvidas_, QString Jnombre_,QObject *parent = nullptr); // Constructor de personaje
    QPixmap *pintor; // almacena el sprite
    double filas,columnas;//eje de imagen
    double ancho,alto; // Tamaño de la imagen
    QRectF boundingRect() const;// Reservar los parametros del sprite
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); // Pinta el pixmap

    //propiedades del personaje
    int Jvidas, Jnivel; // entrega vidas y el nivel respectivo
    QString Jnombre; // entrega el nombre del personaje
    int x,y; // ejes de posicion del personaje

    QTimer *Caer,*Salto; // Funciones que remplazan la fisica

    int brinco;// Limita el salto del personaje
    unsigned doblesalto; // evita que el personaje salte mas de 2 veces
    bool parabolicoderecho=true,parabolicoizquierdo=true;//Respectivos saltos parabolicos (cuando se mueve horizontalmente)
    bool moviendose;//determina si el personaje se mueve o no.

    bool Derecha=false,Izquierda=false;

    int piso=644;//determina la posicion limite del personaje en y (varia en caso de pisar plataformas)

    bool ActivarMovimientos=false;
    void voltear();
    ~Personajes();

    QTimer *Movimientos;
    bool MDerecha=false,MIzquierda=false,MArriba=false,MAbajo=false;

    bool Segundo=false;

    bool soy=true;



public slots:

    void caer(); // Funcion que mantiene el jugador sujeto a el piso
    void salto(); // Funcion que permite (hasta donde la fisica lo permite) el movimiento vertical de el personaje
    void movimientos();

};

#endif // PERSONAJES_H
