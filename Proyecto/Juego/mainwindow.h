#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#define RUTA_ARCHIVO "/Users/camiloalfonsogutierrezhernandez/Desktop/Juego(Delaju)/jugadores.txt"  //tendra el registro de los jugadores.

#include"librerias.h"
#include"nivel.h"
#include"personajes.h"
#include "items.h"

namespace Ui {class MainWindow;}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void conexionuno(); // Se activa si se presiona el botón Bnueva (nueva partida), permite que la función Partida funcione para nuevos jugadores.
    void conexiondos(); // Se activa si se presiona el botón Bcargar (cargar partida), permite que la función Partida funcione para jugadores ya registrados .
    void Partida(); // Funge como un menú de acceso para el usuario; en función del botón presionado(Bnueva o Bcargar), recibe los datos de una nueva partida o de una ya existente.
    void confirmarPartida(); // Determina en función  del botón presionado con anterioridad (Bnueva o Bcargar) si se crea una nueva partida o carga una ya existente.

    void estadisticas(); // Menú donde se muestran los 5 mejores puntajes  de los jugadores de mayor nivel, se despliega al presionar el botón Branking.


    void devolverse(); // Botón que permite al usuario retornar al menú principal, al presionar el botón regresar.

    void multijugador(); // Botón para iniciar el modo multijugador al presionar el botón Bmultijugador.
    void desaparecermultijugador(); // Función encargada de desaparecer y aparecer items en el modo multijugador.

    void actualizarTiempo(); // Actualiza el tiempo de los cronómetros (QLCDnumber) de cada nivel.

    void Colisiones(); // Función encargada de definir los limites del escenario y puerta final, también se encarga de la gestión de los datos del usuario una ves este termina o pierde el nivel.

    void arduidos(); // Función encargada de implementar Arduino como mecánica del juego.

    void maquinavectores();
    void disparando();
    void esferando();
    void pendulo();


private:

    void Juego(QString); // Esta función se encarga de recibir, los elementos básicos del juego, Nivel, Cronometro, Items, etc.
    int Dato(QString); // Pendiente.


    void keyPressEvent(QKeyEvent * evento);
    void keyReleaseEvent(QKeyEvent *evento);


private:

    Ui::MainWindow *ui;

    QGraphicsScene *menu,*partida=new QGraphicsScene(0,0,1280,720),*ranking,*findeljuego; // Escenas  principales que constituyen la interfaz  grafica del menú.
    QPushButton *Bnueva,*Bcargar,*Branking,*Bmultijugador,*Aceptar,*regresar; //botones para accionar funciones respectivas en interfaz  grafica del menú.
    bool NuevaOCargar= false; // Determina si se crea una nueva partida o se carga una ya existente

    QLabel *letrero,*letrero2; // Label que tienen  escrito : "usuario y contraseña" respectivamente.
    QLineEdit *letrero3,*letrero4; // Label que recibe : "usuario y contraseña" respectivamente.
    QLabel *existe=new QLabel,*existe2=new QLabel; // Label encargados de imprimir las respectivas advertencias a la hora de ingresar usuario y contraseña.

    QString contenido,auxiliar,DatosJugadorAuxiliar; // Tendra almacenado el contenido de el archivo txt definido en linea 3 de mainWindow.h.

    nivel *uno; // Objeto  Nivel
    Personajes *Principal; // Personaje principal

    QTimer *cronometro=new QTimer; // Timer que inicializa la función Actualizartiempo.
    QLCDNumber *Minutos,*Segundos,*Milesimas; // QLCDNumber's que almacenan Minutos, Segundos y centésimas (Estas ultimas escritas como Milésimas).

    QTimer *colision; // Timer que inicializa la función Colisiones.

    QPixmap *GraficoItem;

    QTimer *Arduino; // Timer que inicializa la función arduidos.
    QSerialPort serial;
    bool arduino=false; // Variable en cargada de determinar si Arduino esta conectado o no.
    int bonus=3; // Determina el numero de interacciones del Arduino.

    bool BloquearTeclado=false; // Determina cuando activar los eventos de teclado


    QTimer *TrampasMultijugador; // Activa  la función desaparecermultijugador
    int DesaparecerItemsMultijugador=0; // Determina la acción a la que se debe de recurrir en la función desaparecermultijugador
    int TiempoDedesaparicion=1700;

    items *plataforma5,*plataforma6,*plataforma7,*plataforma8,*plataforma9,*plataforma10,*plataforma11;
    items *plataformaM5,*plataformaM6;
    items *matriz,*matriz2;
    items *Operaciones,*Operaciones2,*Operaciones3,*Operaciones4;

    QGraphicsPixmapItem *examen, *examen2;
    QGraphicsPixmapItem *ExamenSwitch,*ExamenSwitch2;

    items *Limite;

    QTimer *MaquinaVectores;
    QTimer *Disparo;
    QTimer *Esfera;
    QTimer *Pendulo;

     int Nivelito=0;

    bool bandera=true;
    bool bandera2=true;

    QMediaPlayer *musica; // Almacena el sonido del menu
    QMediaPlayer *MusicaNivel; // Almacena el sonido de los niveles
    QMediaPlayer *SaltoSonido; // Almacena el sonido del salto del personaje

   QGraphicsLineItem *Cuerda;


};
#endif // MAINWINDOW_H
