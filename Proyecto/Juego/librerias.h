#ifndef LIBRERIAS_H
#define LIBRERIAS_H

#include <QGraphicsItem>

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPixmap>
#include <QPushButton>
#include <QLabel>
#include <QPlainTextEdit>
#include <QFile>
#include <QLineEdit>
#include <QLCDNumber>
#include <QTimer>
#include <QKeyEvent>
#include <QObject>
#include <math.h>
#include <QList>
#include <QString>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include<QMediaPlayer>
#endif // LIBRERIAS_H
