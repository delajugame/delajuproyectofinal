#ifndef ITEMS_H
#define ITEMS_H
#include"librerias.h"

class items: public QObject, public QGraphicsItem

{
private:
      int Amplitud=207;
      double pulsacion=sqrt(10/207);
     Q_OBJECT
public:
    int ancho,alto,x,y;
    items(int, int, int, int,bool,bool,bool,QPixmap *painter, QObject *parent);
    //pintar
    double filas=0,columnas=0;
    QPixmap *pintor;

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);

    bool Movibles;
    QTimer *MovimientoItem;
    bool vertical,devuelve,quieto=false;
    int inicial,final;

    double velocidad=1;

    int tipo=1;

    double Ax,Vx;

    bool esfera=false;

public slots:
    void MoverItem();


};

#endif // ITEMS_H
