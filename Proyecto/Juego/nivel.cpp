#include "nivel.h"
#include<QDebug>
nivel::nivel(int p, int p2, int p3, int p4, Personajes *Jugadorcito){

    Px1=p;
    Py1=p2;
    Px2=p3;
    Py2=p4;
    jugador=Jugadorcito;
    multijugador=jugador;
    jugador->x=Px1;
    jugador->y=Py1;

    jugador->filas=0;
    jugador->columnas=186.4;
    jugador->setPos(Px1,Py1);

    nombre=jugador->Jnombre;
    vidas=jugador->Jvidas;
    Datos=new QLabel();

    Datos->setGeometry(18,13,133,44);//Ubicando datos del usuario
    Datos->setText(nombre+" : "+QString::number(vidas)); //concatena nombres con numero de vidas

    Datos->setStyleSheet("QLabel { background-color : transparent; color : black; }"); //Hace trasparente el fondo de los botones
    Datos->setFont(QFont("Bauhaus 93",30)); // Se designa una fuente parala letra
    Datos->adjustSize();

    //se crea una nueva escena y se determinan sus parametros
    escenario=new QGraphicsScene(0,0,1280,720);
    escenario->addWidget(Datos);

    //se crea la puerta de llegada
    puerta=new QGraphicsLineItem(2220,665,1240,665);
    escenario->addItem(puerta);



    //se hace el foco al jugador para poder implementar los eventos de teclado.

    escenario->addItem(jugador);

    //Espacio de juego
    izquierda=new QGraphicsLineItem(0,0,0,740);
    derecha=new QGraphicsLineItem(1280,0,1280,740);
    abajo=new QGraphicsLineItem(0,665,1280,665);
    escenario->addItem(izquierda);
    escenario->addItem(derecha);
    escenario->addItem(abajo);

    Obstaculos=new QTimer;
    Obstaculos->start(10);
    connect(Obstaculos,SIGNAL (timeout()),SLOT(Obstaculizando()));

    Plataformas=new QTimer;
    Plataformas->start(5);
    connect(Plataformas,SIGNAL (timeout()),SLOT(Plataformando()));

    Bonificaciones=new QTimer;
    Bonificaciones->start(5);
    connect(Bonificaciones,SIGNAL (timeout()),SLOT(Bonificacionando()));



}

nivel::nivel(int p, int p2, int p3, int p4, int p5 , int p6, Personajes* Player1, Personajes* Player2){


    SonDos=true;

    Px1=p;
    Py1=p2;

    Px2=p3;
    Py2=p4;

    Px3=p5;
    Py3=p6;

    escenario=new QGraphicsScene(0,0,1280,720);

    jugador=Player1;
    multijugador=Player2;

    jugador->x=Px1;
    jugador->y=Py1;

    jugador->filas=0;
    jugador->columnas=186.4;
    jugador->setPos(Px1,Py1);


    nombre=jugador->Jnombre;
    vidas=jugador->Jvidas;

    puerta=new QGraphicsLineItem(640,301,692,301);
    escenario->addItem(puerta);


    escenario->addItem(jugador);


    multijugador->x=Px3;
    multijugador->y=Py3;

    multijugador->filas=0;
    multijugador->columnas=186.4;
    multijugador->setPos(Px3,Py3);

    escenario->addItem(multijugador);


    nombre2=multijugador->Jnombre;
    vidas2=multijugador->Jvidas;

    Datos=new QLabel();
    Datos->setGeometry(18,10,133,44);//Ubicando datos del usuario
    Datos->setText(nombre+" : "+QString::number(vidas)); //concatena nombres con numero de vidas
    Datos->setStyleSheet("QLabel { background-color : transparent; color : black; }"); //Hace trasparente el fondo de los botones
    Datos->setFont(QFont("Bauhaus 93",30)); // Se designa una fuente parala letra
    Datos->adjustSize();

    Datos2=new QLabel();
    Datos2->setGeometry(1000,10,133,44);//Ubicando datos del usuario
    Datos2->setText(nombre2+" : "+QString::number(vidas2)); //concatena nombres con numero de vidas
    Datos2->setStyleSheet("QLabel { background-color : transparent; color : black; }"); //Hace trasparente el fondo de los botones
    Datos2->setFont(QFont("Bauhaus 93",30)); // Se designa una fuente parala letra
    Datos2->adjustSize();

    escenario->addWidget(Datos);
    escenario->addWidget(Datos2);

    izquierda=new QGraphicsLineItem(0,0,0,740);
    derecha=new QGraphicsLineItem(1280,0,1280,740);
    abajo=new QGraphicsLineItem(0,665,1280,665);
    escenario->addItem(izquierda);
    escenario->addItem(derecha);
    escenario->addItem(abajo);

    Obstaculos=new QTimer;
    Obstaculos->start(10);
    connect(Obstaculos,SIGNAL (timeout()),SLOT(Obstaculizando()));

    Plataformas=new QTimer;
    Plataformas->start(5);
    connect(Plataformas,SIGNAL (timeout()),SLOT(Plataformando()));

    Bonificaciones=new QTimer;
    Bonificaciones->start(5);
    connect(Bonificaciones,SIGNAL (timeout()),SLOT(Bonificacionando()));


}

nivel::~nivel()
{
    delete jugador;
    delete escenario;

    for(int i=0;i<obstaculos.size();i++){
       (obstaculos.at(i)->~items());
    }
    for(int i=0;i<bonificaciones.size();i++){
       (bonificaciones.at(i)->~items());
    }
    for(int i=0;i<plataformas.size();i++){
       (plataformas.at(i)->~items());
    }
}

void nivel::ActualizarItems(){
    for(int i=0;i<obstaculos.size();i++){
       escenario->addItem(obstaculos.at(i));// añade los items al escenario
    }
    for(int i=0;i<bonificaciones.size();i++){
       escenario->addItem(bonificaciones.at(i));
    }
    for(int i=0;i<plataformas.size();i++){
       escenario->addItem(plataformas.at(i));
    }
}
void nivel::darItems(items *trampa, int X){ // se añaden items a una lista de items respectiva
    switch (X) {
    case 1:obstaculos.append(trampa);break;
    case 2:bonificaciones.append(trampa);break;
    case 3:plataformas.append(trampa);break;
    }


}

void nivel::bonificar(items *Ayuda){
    switch (Ayuda->tipo) {
    case 1:jugador->Jvidas+=2;vidas+=2;Datos->setText(nombre+" : "+QString::number(vidas));break;//aumento vidas
    case 2:for(int i=0;i<obstaculos.size();i++){obstaculos.at(i)->velocidad/=2;};break;//trampas mas lentas
    case 3:obstaculos.at(obstaculos.size()-1)->setPos(5000,0);break;//desaparecer item
    case 4:jugador->x=Px2-100;break;//teletransportacion
    case 5:{CheckPoint=true;Px1=jugador->x;Py1=jugador->y;break;}//check point
    case 6:jugador->Jvidas-=4;vidas-=4;Datos->setText(nombre+" : "+QString::number(vidas));break;
    case 7:for(int i=0;i<obstaculos.size();i++){obstaculos.at(i)->velocidad*=2;};break;
    case 8:for(int i=0;i<obstaculos.size();i++){if(obstaculos.at(i)->Movibles && obstaculos.at(i)->vertical && obstaculos.at(i)->devuelve){jugador->x=obstaculos.at(i)->x;jugador->y=644;jugador->setPos(jugador->x,644);break;}};break;//ubica usuario debajo de trampa movible
    case 9:jugador->x=47;jugador->y=100;jugador->setPos(100,200);jugador->ActivarMovimientos=false;break;
    case 10:jugador->ActivarMovimientos=false;break;
    case 11:jugador->x=1058;jugador->y=100;jugador->setPos(1058,100);break;
    }
}

void nivel::Obstaculizando(){//falta para los demas items (y Mensaje)
   if(!SonDos) {
        for(int i=0;i<obstaculos.size();i++){
            if(obstaculos.at(i)->collidesWithItem(jugador)){
                if(!CheckPoint)jugador->ActivarMovimientos=false;
                jugador->doblesalto=0;
                jugador->Jvidas-=1;
                vidas--;
                Datos->setText(nombre+" : "+QString::number(vidas));
                jugador->x=Px1;
                jugador->y=Py1;
                jugador->setPos(Px1,Py1);
                for(int j=0;j<obstaculos.size();j++){
                    if(obstaculos.at(i)->Movibles){
                        if(obstaculos.at(i)->vertical)obstaculos.at(i)->y=obstaculos.at(i)->inicial;
                        else obstaculos.at(i)->x=obstaculos.at(i)->inicial;
                    }
                }
            }
        }
    }
   else{
       for(int i=0;i<obstaculos.size();i++){
           if(obstaculos.at(i)->collidesWithItem(multijugador)){
               if(!CheckPoint)multijugador->ActivarMovimientos=false;
               multijugador->doblesalto=0;
               multijugador->Jvidas-=1;
               vidas2--;
               Datos2->setText(nombre+" : "+QString::number(vidas2));
               multijugador->x=Px3;
               multijugador->y=Py3;
               multijugador->setPos(Px3,Py3);
               for(int j=0;j<obstaculos.size();j++){
                   if(obstaculos.at(i)->Movibles){
                       if(obstaculos.at(i)->vertical)obstaculos.at(i)->y=obstaculos.at(i)->inicial;
                       else obstaculos.at(i)->x=obstaculos.at(i)->inicial;
                   }
               }
           }
       }
       for(int i=0;i<obstaculos.size();i++){
           if(obstaculos.at(i)->collidesWithItem(jugador)){
               if(!CheckPoint)jugador->ActivarMovimientos=false;
               jugador->doblesalto=0;
               jugador->Jvidas-=1;
               vidas--;
               Datos->setText(nombre+" : "+QString::number(vidas));
               jugador->x=Px1;
               jugador->y=Py1;
               jugador->setPos(Px1,Py1);
               for(int j=0;j<obstaculos.size();j++){
                   if(obstaculos.at(i)->Movibles){
                       if(obstaculos.at(i)->vertical)obstaculos.at(i)->y=obstaculos.at(i)->inicial;
                       else obstaculos.at(i)->x=obstaculos.at(i)->inicial;
                   }
               }
           }
       }

   }
}

void nivel::Plataformando(){
    int cont=0;

    int posx=jugador->x;

    for (int cont2=0;cont2<plataformas.size();cont2++) {


        if(plataformas.at(cont)->collidesWithItem(jugador)){
           if(plataformas.at(cont)->Movibles ) {
                if(plataformas.at(cont)->Movibles && !plataformas.at(cont)->vertical  && plataformas.at(cont)->y-34==jugador->y){
                    jugador->x=posx;

                    jugador->doblesalto=0;
                    jugador->brinco=0;

                    jugador->piso=plataformas.at(cont)->y-plataformas.at(cont)->alto-24;



                    if(plataformas.at(cont)->x<plataformas.at(cont)->final && plataformas.at(cont)->devuelve ){

                        jugador->x++;
                        jugador->setPos(jugador->x,jugador->y);
                    }

                    else if(plataformas.at(cont)->x==plataformas.at(cont)->final){

                        jugador->x--;
                        jugador->setPos(jugador->x,jugador->y);
                    }

                    else if(plataformas.at(cont)->x>plataformas.at(cont)->inicial && !plataformas.at(cont)->devuelve){

                        jugador->x--;
                        jugador->setPos(jugador->x,jugador->y);}

                }

                else
                    if(plataformas.at(cont)->Movibles && plataformas.at(cont)->vertical){
                        jugador->y--;
                        jugador->piso=jugador->y;
                        jugador->setPos(jugador->x,jugador->y);
                    }
           }else{

               if(plataformas.at(cont)->y-34==jugador->y){
                   jugador->doblesalto=0;
                   jugador->brinco=0;

                   jugador->piso=plataformas.at(cont)->y-plataformas.at(cont)->alto-24;
               }
           }

        }else {
            cont=cont+1;
            jugador->piso=644;
        }
    }
    if(SonDos){

        int cont3=0;

        int posx2=multijugador->x;

        for (int cont2=0;cont2<plataformas.size();cont2++) {


            if(plataformas.at(cont3)->collidesWithItem(multijugador)){
               if(plataformas.at(cont3)->Movibles ) {
                    if(plataformas.at(cont3)->Movibles && !plataformas.at(cont3)->vertical  && plataformas.at(cont3)->y-34==multijugador->y){
                        multijugador->x=posx2;

                        multijugador->doblesalto=0;
                        multijugador->brinco=0;

                        multijugador->piso=plataformas.at(cont3)->y-plataformas.at(cont3)->alto-24;



                        if(plataformas.at(cont3)->x<plataformas.at(cont3)->final && plataformas.at(cont3)->devuelve ){

                            multijugador->x++;
                            multijugador->setPos(multijugador->x,multijugador->y);
                        }

                        else if(plataformas.at(cont3)->x==plataformas.at(cont3)->final){

                            multijugador->x--;
                            multijugador->setPos(multijugador->x,multijugador->y);
                        }

                        else if(plataformas.at(cont3)->x>plataformas.at(cont3)->inicial && !plataformas.at(cont3)->devuelve){

                            multijugador->x--;
                            multijugador->setPos(multijugador->x,multijugador->y);}

                    }

                    else
                        if(plataformas.at(cont3)->Movibles && plataformas.at(cont3)->vertical){
                            multijugador->y--;
                            multijugador->piso=multijugador->y;
                            multijugador->setPos(multijugador->x,multijugador->y);
                        }
               }else{
                   if(plataformas.at(cont3)->y-34==multijugador->y){
                       multijugador->doblesalto=0;
                       multijugador->brinco=0;

                       multijugador->piso=plataformas.at(cont3)->y-plataformas.at(cont3)->alto-24;
                   }
               }

            }else {
                cont3=cont3+1;
                multijugador->piso=644;
            }
        }

    }
}

void nivel::Bonificacionando(){//tipo (nivel) y aca coliciona y llama funcion de items
    for(int i=0;i<bonificaciones.size();i++){
        if(jugador->collidesWithItem(bonificaciones.at(i))){
            bonificaciones.at(i)->setPos(5000,8000);
            bonificar(bonificaciones.at(i));

        }
    }
}
