#include "personajes.h"
#include<QDebug>
//

Personajes::Personajes(int Jvidas_, QString Jnombre_, QObject *parent) : QObject(parent){ // Constructor
    // Datos que constituyen al personaje

    Jvidas=Jvidas_;
    Jnombre=Jnombre_;
    y=644;
    x=40;
    //Informacion para que el sprite funcione correctamente
    filas = 0;
    columnas = 0;
    alto=49;
    ancho=46.5;
    pintor = new QPixmap(":/IMG/sprites/Personaje3.png");
    this->update(-ancho/2,-alto/2,ancho,alto);
    setPos(x,y);

    // Parametros fisicos
    piso=y;
    brinco=0; 

    //conjunto de timer y slots que conectan con las funciones que permiten al jugador tener un comportamiento fisico
    Caer=new QTimer;
    Salto=new QTimer;
    Caer->start(5);
    connect(Caer,SIGNAL (timeout()),SLOT(caer()));
    connect(Salto,SIGNAL (timeout()),SLOT(salto()));

    Movimientos=new QTimer;
    Movimientos->start(34);
    connect(Movimientos,SIGNAL (timeout()),SLOT (movimientos()));
}

void Personajes::caer(){
    if(y<piso){
        y++;
        brinco--;
        if(parabolicoderecho && !Derecha)x+=1;
        if(parabolicoizquierdo && !Izquierda)x-=1;
        setPos(x,y);
        if(y==piso){ ActivarMovimientos=true;doblesalto=0;parabolicoderecho=false;parabolicoizquierdo=false;}
    }
}
void Personajes::salto(){
    brinco+=3;
    y-=3;
    if(parabolicoderecho && !Derecha)x+=1;
    if(parabolicoizquierdo && !Izquierda)x-=1;
    setPos(x,y);
    if(brinco<49)Salto->start(5);
    else Salto->stop();
}

void Personajes::movimientos(){
    if(MDerecha && !Derecha){
        if(ActivarMovimientos){
            parabolicoderecho=true;
            parabolicoizquierdo=false;
            x+=7;
            setPos(x,y);
        }
        filas=0;
        columnas+=46.6;
        if(columnas>93.2)columnas=0;
        this->update(-ancho/2,-alto/2,ancho,alto);
    }
    if(MIzquierda && !Izquierda){
        if(ActivarMovimientos) {
             parabolicoderecho=false;
             parabolicoizquierdo=true;
             x-=7;
             setPos(x,y);
         }
         filas=49;
         columnas+=46.6;
         if(columnas>93.2)columnas=0;
         this->update(-ancho/2,-alto/2,ancho,alto);
    }
    if(MArriba){
        MArriba=false;
        doblesalto++;
        if(doblesalto<3){
            brinco=0;
            Salto->start(5);
        }
    }
    if(MAbajo){
        moviendose=false;
        MIzquierda=false;
        MDerecha=false;
        columnas=139.8;
        this->update(-ancho/2,-alto/2,ancho,alto);
    }
}


QRectF Personajes::boundingRect() const{return QRectF(-ancho/2,-alto/2,ancho-6.2,alto);}
void Personajes::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *){painter->drawPixmap(-int(ancho/2),int(-alto/2),*pintor,int(columnas),int(filas),int(ancho),int(alto));}

void Personajes::voltear()
{
    filas=49;
    columnas=186.4;
    this->update(-ancho/2,-alto/2,ancho,alto);
}

Personajes::~Personajes(){
    delete pintor;
    delete Caer;
    delete Salto;
}
