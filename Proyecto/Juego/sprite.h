#ifndef SPRITE_H
#define SPRITE_H
#include"librerias.h"
class sprite : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit sprite(QObject *parent = nullptr);

    QPixmap *pintor;

    double filas,columnas;//eje de imagen
    double ancho,alto;

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);


signals:

public slots:
    void keyPressEvent(QKeyEvent * evento);
};

#endif // SPRITE_H
