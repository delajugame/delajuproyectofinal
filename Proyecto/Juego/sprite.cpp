#include "sprite.h"
#include<QDebug>
sprite::sprite(QObject *parent) : QObject(parent){
    filas = 0;
    columnas = 0;
    pintor = new QPixmap(":/IMG/sprites/Personaje.png");
    alto=49;
    ancho=46.5;
    this->update(-ancho/2,-alto/2,ancho,alto);

}

void sprite::keyPressEvent(QKeyEvent *evento){
    if(evento->key() == Qt::Key_Right){
        filas=0;
        columnas+=46.5;
        if(columnas>93){
            columnas=0;
        }
        this->update(-ancho/2,-alto/2,ancho,alto);

    }else{
        if(evento->key()==Qt::Key_Left){
            filas=49;
            columnas+=46.5;
            if(columnas>93){
                columnas=0;
            }
            this->update(-ancho/2,-alto/2,ancho,alto);
        }
    }
}
QRectF sprite::boundingRect() const{
    return QRectF(-ancho/2,-alto/2,ancho,alto);
}

void sprite::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->drawPixmap(-int(ancho/2),int(-alto/2),*pintor,int(columnas),int(filas),int(ancho),int(alto));
}








