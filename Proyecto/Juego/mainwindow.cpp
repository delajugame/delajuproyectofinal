#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)

// Menu ) -----------------------------------------------------------------------------------------

{
    musica=new QMediaPlayer(this);
    musica->setMedia(QUrl("qrc:/SOUND/sound/melodyloops-the-quiet-solitude.mp3"));
    musica->setVolume(7);
    musica->play();

    SaltoSonido=new QMediaPlayer(this);
    SaltoSonido->setMedia(QUrl("qrc:/SOUND/sound/jumping-mike-mario.mp3"));
    SaltoSonido->setVolume(100);

    MusicaNivel=new QMediaPlayer(this);
    MusicaNivel->setMedia(QUrl("qrc:/SOUND/sound/Automation.mp3"));
    MusicaNivel->setVolume(7);


    ui->setupUi(this);
    //escena de menu principal
    menu=new QGraphicsScene(0,0,1280,720);
    ui->graphicsView->setScene(menu);
    ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/Menu.png"));


    //definicion del boton nueva partida
    Bnueva=new QPushButton();
    menu->addWidget(Bnueva);
    Bnueva->setGeometry(640,300,150,50);
    Bnueva->setText("Nueva Partida");
    Bnueva->setFont(QFont("Ebrima"));

    //definicion del boton cargar
    Bcargar=new QPushButton();
    menu->addWidget(Bcargar);
    Bcargar->setGeometry(640,375,150,50);
    Bcargar->setText("Cargar Partida");
    Bcargar->setFont(QFont("Ebrima"));

    //definicion del boton ranking
    Branking=new QPushButton();
    menu->addWidget(Branking);
    Branking->setGeometry(640,450,150,50);
    Branking->setText("Ranking");
    Branking->setFont(QFont("Ebrima"));

    //definicion del boton Multijugador
    Bmultijugador=new QPushButton;
    menu->addWidget(Bmultijugador);
    Bmultijugador->setText("");
    Bmultijugador->setGeometry(940,320,280,210);
    Bmultijugador->setStyleSheet("QPushButton { background-color : transparent; color : red; }");


    //labels de advertencia
    existe->setGeometry(810,300,150,50);
    existe->setFont(QFont("Eras Demi ITC",10));
    existe->setStyleSheet("QLabel { background-color : transparent; color : red; }");
    partida->addWidget(existe);
    existe2->setGeometry(810,375,500,50);
    existe2->setStyleSheet("QLabel { background-color : transparent; color : red; }");
    existe2->setFont(QFont("Eras Demi ITC",10));
    partida->addWidget(existe2);


    //llamado de las funciones de los respectivos botones
    connect(Bnueva,SIGNAL (released()),this, SLOT (conexionuno()));
    connect(Bcargar,SIGNAL (released()),this, SLOT (conexiondos()));
    connect(Branking,SIGNAL (released()),this,SLOT (estadisticas()));
    connect(Bmultijugador,SIGNAL (released()),this,SLOT (multijugador()));

}


// Menu partida ) ---------------------------------------------------------------------------------
void MainWindow::Partida(){


    //se añade la escena de partida a el graphicsView y se le asigna una imagen de fondo
    ui->graphicsView->setScene(partida);
    ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/Menu.png"));

    // se crea el boton de regreso al menu
    regresar=new QPushButton;
    regresar->setGeometry(180,320,150,30);
    regresar->setStyleSheet("QPushButton { background-color : transparent; border : none; }");

    // se le asigna una imagen y se ubica en un posicion
    QPixmap flechita(":/IMG/sprites/Flecha.png");
    QIcon ButtonIcon(flechita);
    regresar->setIcon(ButtonIcon);
    regresar->setIconSize(flechita.rect().size());
    regresar->adjustSize();
    partida->addWidget(regresar);

    // Se conecta el boton de regreso con su respectiva funcion (devuelve al menu principal)
    connect(regresar,SIGNAL (released()),this,SLOT (devolverse()));


    // se crea el letrero 'label' correspondiente para nombre de usuario.
    letrero=new QLabel();
    letrero->setText("    Usuario: ");
    letrero->setFont(QFont("Ebrima",15));
    letrero->adjustSize();
    partida->addWidget(letrero);
    letrero->setGeometry(500,300,150,50);


    // se crea el letrero 'label' correspondiente para nombre de contraseña.
    letrero2=new QLabel();
    letrero2->setText("    Contraseña: ");
    letrero2->setFont(QFont("Ebrima",15));
    letrero2->adjustSize();
    partida->addWidget(letrero2);
    letrero2->setGeometry(500,375,150,50);

    // input para nombre de usuario
    letrero3=new QLineEdit;
    letrero3->setPlaceholderText("Nombre de usuario...");
    letrero3->setMaxLength(20);
    letrero3->setGeometry(650,300,150,50);
    partida->addWidget(letrero3);

    // input contraseña
    letrero4=new QLineEdit;
    letrero4->setPlaceholderText("Clave...");
    letrero4->setMaxLength(20);
    letrero4->setEchoMode(QLineEdit::Password);
    letrero4->setGeometry(650,375,150,50);
    partida->addWidget(letrero4);

    // boton de confirmar
    Aceptar=new QPushButton();
    Aceptar->setGeometry(580,450,150,50);
    Aceptar->setText("Confirmar");
    Aceptar->setFont(QFont("Arial Black",15));
    partida->addWidget(Aceptar);

    //reiniciando letreros
    existe->clear();
    existe2->clear();


    // concecta el boton con la funcion confirmarPartida
    connect(Aceptar,SIGNAL (released()),this, SLOT (confirmarPartida()));

}


// Confirmar partida) -----------------------------------------------------------------------------
void MainWindow::confirmarPartida(){//implementacion del boton aceptar (nueva partida)

    // leer el el archivo
    bool confirmar;//evita que se escriba en el txt un usuario existente
    QFile leer(RUTA_ARCHIVO);
    leer.open(QIODevice::ReadOnly);
    contenido="pepa ping";

    confirmar=false;//determina si el usuario esta o no en la lista de registro
    //itera sobre documento
    while(contenido.length()!=0){

        //obtener nombre de usurio
        contenido=leer.readLine();
        auxiliar=contenido;
        int n=contenido.indexOf(" ");
        contenido.remove(0,n+1);
        n=contenido.indexOf(" ");

        if(letrero3->text()==contenido.left(n)){//en este caso si existe el nombre de usuario

            //obtener contraseña de usuario (existente)
            contenido.remove(0,n+1);
            n=contenido.indexOf(" ");

            //determina que mensaje escribir en los letreros
            if(NuevaOCargar){existe->setText("Usuario ya creado.");existe2->clear();qDebug()<<"Entre Aqui 1";}// Nueva partida
            else {existe->setText("Usuario correcto");existe2->clear();qDebug()<<"Entre Aqui 2";}

            confirmar=true;//verifica si el usuario esta en la lista (para luego empezar juego)

            if(letrero4->text()==contenido.left(n)){//la contraseña pertenece al usuario


                if(!NuevaOCargar)Juego(auxiliar);//cargar partida (juego)
                else {existe2->setText("clave existente.");qDebug()<<"Entre Aqui 3";}

            }else if(!NuevaOCargar){
                existe2->setText("la clave es incorrecta");qDebug()<<"Entre Aqui 4";
            }
        }

    }
    if(!confirmar){
        if(NuevaOCargar){//anulamos el caracter espacio para evitar errores en la lectura de DatosDeUsuario
            if(letrero3->text().indexOf(" ")!=-1 || letrero3->text().indexOf("ñ")!=-1 || letrero3->text().indexOf("Ñ")!=-1){
                existe->setText("Caracter no valido");qDebug()<<"Entre Aqui 5";

            }else{
                if(letrero4->text().length()<=0){
                     existe2->setText("Sin Clave.");qDebug()<<"Entre Aqui 6";
                }else{
                    if(letrero4->text().indexOf(" ")!=-1 ||letrero4->text().indexOf("ñ")!=-1 || letrero4->text().indexOf("Ñ")!=-1){
                        existe2->setText("Caracter no valido");qDebug()<<"Entre Aqui 7";
                    }else{
                        if(letrero2->text().length()<=0){
                            existe->setText("Sin Usuario.");qDebug()<<"Entre Aqui 8";
                        }else{
                            leer.close();
                            //Escribe usuario nuevo en txt
                            QFile escribir(RUTA_ARCHIVO);
                            escribir.open(QIODevice::Append);
                            QString Datos="00:00.00 "+letrero3->text()+" "+letrero4->text()+" 1 5\n";//datos de usuario nuevo (valores por defecto)
                            QTextStream stream( &escribir );
                            stream << Datos;
                            escribir.close();
                            Juego(Datos);//inicia el Juego
                        }
                    }
                }
            }
        }else{
             existe->setText("Usuario inexistente");
        }
    }
}


// Estadisticas ) ---------------------------------------------------------------------------------
void MainWindow::estadisticas(){

    // Parametros de la nueva escena
    ranking=new QGraphicsScene(0,0,1280,720);
    ui->graphicsView->setScene(ranking);
    ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/MenuRanking.jpg"));
    QLabel *usuario=new QLabel,*puntaje=new QLabel,*usuario1=new QLabel,*puntaje1=new QLabel,*usuario2=new QLabel,*puntaje2=new QLabel,*usuario3=new QLabel,*puntaje3=new QLabel,*usuario4=new QLabel,*puntaje4=new QLabel,*usuario5=new QLabel,*puntaje5=new QLabel;

    QPalette Pal(palette());
    Pal.setColor(QPalette::Background,Qt::transparent);

    //se crea el boton de regreso al men
    regresar=new QPushButton;
    regresar->setGeometry(180,320,150,30);
    regresar->setStyleSheet("QPushButton { background-color : transparent; border : none; }");

    // se le asigna una imagen y se ubica en un posicion
    QPixmap flechita(":/IMG/sprites/Flecha.png");
    QIcon ButtonIcon(flechita);
    regresar->setIcon(ButtonIcon);
    regresar->setIconSize(flechita.rect().size());
    regresar->adjustSize();
    ranking->addWidget(regresar);

    // Se conecta el boton de regreso con su respectiva funcion (devuelve al menu principal)
     connect(regresar,SIGNAL (released()),this,SLOT (devolverse()));


    // Letreros "usuario" y "tiempo"
    usuario->setGeometry(520,250,200,120);
    usuario->setText("USUARIO ");
    usuario->setFont(QFont("Informal Roman", 50));
    usuario->adjustSize();
    puntaje->setGeometry(900,250,300,100);
    puntaje->setText("TIEMPO");
    puntaje->setFont(QFont("Informal Roman", 50));
    puntaje->adjustSize();
    puntaje->setPalette(Pal);
    usuario->setPalette(Pal);
    ranking->addWidget(usuario);
    ranking->addWidget(puntaje);

    //cargar informacion del archivo
    QFile leer(RUTA_ARCHIVO);
    leer.open(QIODevice::ReadOnly);

    //Se imprimen los puntajes de los 5 primeros usuario
    contenido=leer.readLine();
    usuario1->setGeometry(550,330,300,100);
    puntaje1->setGeometry(900,330,300,100);
    puntaje1->setText(contenido.left(contenido.indexOf(" ")));
    puntaje1->setPalette(Pal);
    puntaje1->setFont(QFont("Blackoak Std", 15));
    puntaje1->adjustSize();
    ranking->addWidget(puntaje1);
    contenido.remove(0,contenido.indexOf(" ")+1);
    usuario1->setText("1. "+contenido.left(contenido.indexOf(" ")));
    usuario1->setFont(QFont("Algerian",25));
    usuario1->adjustSize();
    usuario1->setPalette(Pal);
    ranking->addWidget(usuario1);

    contenido=leer.readLine();
    usuario2->setGeometry(550,380,300,100);
    puntaje2->setGeometry(900,380,300,100);
    puntaje2->setText(contenido.left(contenido.indexOf(" ")));
    puntaje2->setFont(QFont("Blackoak Std", 15));
    puntaje2->adjustSize();
    puntaje2->setPalette(Pal);
    ranking->addWidget(puntaje2);
    contenido.remove(0,contenido.indexOf(" ")+1);
    usuario2->setText("2. "+contenido.left(contenido.indexOf(" ")));
    usuario2->setFont(QFont("Algerian",25));
    usuario2->adjustSize();
    usuario2->setPalette(Pal);
    ranking->addWidget(usuario2);

    contenido=leer.readLine();
    usuario3->setGeometry(550,430,300,100);
    puntaje3->setGeometry(900,430,300,100);
    puntaje3->setText(contenido.left(contenido.indexOf(" ")));
    puntaje3->setFont(QFont("Blackoak Std", 15));
    puntaje3->adjustSize();
    puntaje3->setPalette(Pal);
    ranking->addWidget(puntaje3);
    contenido.remove(0,contenido.indexOf(" ")+1);
    usuario3->setText("3. "+contenido.left(contenido.indexOf(" ")));
    usuario3->setFont(QFont("Algerian",25));
    usuario3->adjustSize();
    usuario3->setPalette(Pal);
    ranking->addWidget(usuario3);

    contenido=leer.readLine();
    usuario4->setGeometry(550,480,300,100);
    puntaje4->setGeometry(900,480,300,100);
    puntaje4->setText(contenido.left(contenido.indexOf(" ")));
    puntaje4->setFont(QFont("Blackoak Std", 15));
    puntaje4->adjustSize();
    puntaje4->setPalette(Pal);
    ranking->addWidget(puntaje4);
    contenido.remove(0,contenido.indexOf(" ")+1);
    usuario4->setText("4. "+contenido.left(contenido.indexOf(" ")));
    usuario4->setFont(QFont("Algerian",25));
    usuario4->adjustSize();
    usuario4->setPalette(Pal);
    ranking->addWidget(usuario4);

    contenido=leer.readLine();
    usuario5->setGeometry(550,530,300,100);
    puntaje5->setGeometry(900,530,300,100);
    puntaje5->setText(contenido.left(contenido.indexOf(" ")));
    puntaje5->setFont(QFont("Blackoak Std", 15));
    puntaje5->adjustSize();
    puntaje5->setPalette(Pal);
    ranking->addWidget(puntaje5);
    contenido.remove(0,contenido.indexOf(" ")+1);
    usuario5->setText("5. "+contenido.left(contenido.indexOf(" ")));
    usuario5->setFont(QFont("Algerian",25));
    usuario5->adjustSize();
    usuario5->setPalette(Pal);
    ranking->addWidget(usuario5);
}


// Devolverse ) -----------------------------------------------------------------------------------
void MainWindow::devolverse(){ // Retorna al menu principal
    bandera=true;
    bandera2=true;
    ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/Menu.png"));
    ui->graphicsView->setScene(menu);
    ui->graphicsView->show();
}


// Conexiones ) -----------------------------------------------------------------------------------
void MainWindow::conexionuno(){
    NuevaOCargar=true;
    Partida();
}
void MainWindow::conexiondos(){
    NuevaOCargar=false;
    Partida();
}


// Multijugador ) ---------------------------------------------------------------------------------
void MainWindow::multijugador(){

    musica->stop();
    MusicaNivel->play();

    QObject *parent = nullptr;
    Personajes *player1,*player2;

    player1=new Personajes(1,"Jugador 1",parent);
    player2=new Personajes(1,"Jugador 2",parent);
    player1->Segundo=false;
    player2->Segundo=true;

    player1->ActivarMovimientos=true;
    player2->ActivarMovimientos=true;

    QPixmap *Pintor=new QPixmap(":/IMG/sprites/Personaje2.png");
    player2->pintor=Pintor;

    uno=new nivel(65,595,665,298,1198,595,player1,player2);

    // Obstaculos -----------------------------------------------------------------

    items *Bloque;
    GraficoItem=new QPixmap(":/IMG/sprites/Cuadro.png");
    Bloque=new items(206,46,635,326,true,true,true,GraficoItem,parent);
    Bloque->inicial=326;
    Bloque->final=644;
    Bloque->velocidad=3;
    uno->darItems(Bloque,1);

    GraficoItem=new QPixmap(":/IMG/sprites/Matriz.png");
    matriz=new items(70,65,300,120,true,true,true,GraficoItem,parent);
    matriz->inicial=100;
    matriz->final=395;
    matriz->velocidad=4;

    matriz2=new items(70,65,980,120,true,true,true,GraficoItem,parent);
    matriz2->inicial=100;
    matriz2->final=395;
    matriz2->velocidad=4;

    // Plataformas no Movibles-----------------------------------------------------------------

    items *plataforma;// derecha abajo
    GraficoItem=new QPixmap(":/IMG/sprites/PlataformaRegla.png");
    plataforma=new items(70,23,900,620,false,false,false,GraficoItem,parent);
    uno->darItems(plataforma,3);

    items *plataforma2;// izquierda abajo
    plataforma2=new items(70,23,440,620,false,false,false,GraficoItem,parent);
    uno->darItems(plataforma2,3);

    items *plataforma3;// derecha medio
    plataforma3=new items(70,23,1065,540,false,false,false,GraficoItem,parent);
    uno->darItems(plataforma3,3);

    items *plataforma4;// izquierda medio
    plataforma4=new items(70,23,215,540,false,false,false,GraficoItem,parent);
    uno->darItems(plataforma4,3);

    // derecha medio aparece
    plataforma5=new items(70,23,980,440,false,false,false,GraficoItem,parent);

    // izquierda medio aparece
    plataforma6=new items(70,23,300,440,false,false,false,GraficoItem,parent);

    // derecha medio superior
    plataforma7=new items(70,23,820,400,false,false,false,GraficoItem,parent);

    // izquierda medio aparece
    plataforma8=new items(70,23,460,400,false,false,false,GraficoItem,parent);





    // Plataformas Movibles--------------------------------------------------------------------

    items *plataformaM;// izquierda vertical movible
    plataformaM=new items(70,23,50,500,true,true,true,GraficoItem,parent);
    plataformaM->inicial=290;
    plataformaM->final=500;
    plataformaM->velocidad=1;
    uno->darItems(plataformaM,3);

    items *plataformaM2;// derecha vertical movible
    plataformaM2=new items(70,23,1230,500,true,true,true,GraficoItem,parent);
    plataformaM2->inicial=290;
    plataformaM2->final=500;
    plataformaM2->velocidad=1;
    uno->darItems(plataformaM2,3);

    items *plataformaM3;// izquierda horizontal movible
    plataformaM3=new items(70,23,180,320,true,false,true,GraficoItem,parent);
    plataformaM3->inicial=180;
    plataformaM3->final=350;
    plataformaM3->velocidad=1;
    uno->darItems(plataformaM3,3);


    items *plataformaM4;// derecha horizontal movible
    plataformaM4=new items(70,23,930,320,true,false,true,GraficoItem,parent);
    plataformaM4->inicial=930;
    plataformaM4->final=1100;
    plataformaM4->velocidad=1;
    uno->darItems(plataformaM4,3);


    // izquierda  inferior horizontal movible, aparece
    plataformaM5=new items(70,23,50,520,true,false,true,GraficoItem,parent);
    plataformaM5->inicial=60;
    plataformaM5->final=150;
    plataformaM5->velocidad=1;



    // derecha   inferior horizontal movible, aparece
    plataformaM6=new items(70,23,1130,520,true,false,true,GraficoItem,parent);
    plataformaM6->inicial=1130;
    plataformaM6->final=1220;
    plataformaM6->velocidad=1;

    // item (Examen)--------------------------------------------------------------------

    QPixmap const GraficoItem2(":/IMG/sprites/ItemExamen.png");
    examen = new QGraphicsPixmapItem();
    examen->setPixmap(GraficoItem2);



    examen2 = new QGraphicsPixmapItem();
    examen2->setPixmap(GraficoItem2);





    //escalera la puerta.

    plataforma9=new items(70,23,460,400,false,false,false,GraficoItem,parent);
    plataforma10=new items(70,23,460,400,false,false,false,GraficoItem,parent);
    plataforma11=new items(70,23,460,400,true,true,true,GraficoItem,parent);

    GraficoItem=new QPixmap(":/IMG/sprites/Suma.png");
    Operaciones=new items(72,72,769,564,true,false,true,GraficoItem,parent);


    GraficoItem=new QPixmap(":/IMG/sprites/Resta.png");
    Operaciones2=new items(72,14,769,434,true,false,true,GraficoItem,parent);


    GraficoItem=new QPixmap(":/IMG/sprites/Multiplicacion.png");
    Operaciones3=new items(72,72,769,304,true,false,true,GraficoItem,parent);


    GraficoItem=new QPixmap(":/IMG/sprites/Division.png");
    Operaciones4=new items(72,72,769,600,true,true,true,GraficoItem,parent);





    ui->graphicsView->setScene(uno->escenario);
    ui->graphicsView->show();
    ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/FondoMultijugador.png"));
    BloquearTeclado=true;

    colision=new QTimer;
    colision->stop();
    colision->start(5);
    connect(colision,SIGNAL(timeout()),SLOT(Colisiones()));

    uno->ActualizarItems();

    TrampasMultijugador=new QTimer;
    connect(TrampasMultijugador,SIGNAL (timeout()),SLOT (desaparecermultijugador()));
    TrampasMultijugador->start(TiempoDedesaparicion);


}


// Desaparecer multijugador ) ---------------------------------------------------------------------
void MainWindow::desaparecermultijugador()
{

    DesaparecerItemsMultijugador++;

    switch (DesaparecerItemsMultijugador) {
    case 1:{
            QObject *parent=nullptr;
            items *chuzos;
            GraficoItem=new QPixmap(":/IMG/sprites/Triangulo.png");
            chuzos=new items(288,28,900,652,false,false,false,GraficoItem,parent);
            uno->darItems(chuzos,1);

            items *chuzos2;
            chuzos2=new items(288,28,1188,652,false,false,false,GraficoItem,parent);
            uno->darItems(chuzos2,1);

            items *chuzos3;
            chuzos3=new items(288,28,82,652,false,false,false,GraficoItem,parent);
            uno->darItems(chuzos3,1);

            items *chuzos4;
            chuzos4=new items(288,28,372,652,false,false,false,GraficoItem,parent);
            uno->darItems(chuzos4,1);

            uno->escenario->addItem(chuzos);
            uno->escenario->addItem(chuzos2);
            uno->escenario->addItem(chuzos3);
            uno->escenario->addItem(chuzos4);



            break;
        }
        case 2:{
            uno->escenario->removeItem(uno->plataformas.at(0));
            uno->escenario->removeItem(uno->plataformas.at(1));

            uno->plataformas.at(0)->setPos(5000,5000);
            uno->plataformas.at(1)->setPos(5000,5000);

            break;
        }
        case 3:{
            uno->escenario->removeItem(uno->plataformas.at(2));
            uno->escenario->removeItem(uno->plataformas.at(3));


            uno->plataformas.at(2)->setPos(5000,5000);
            uno->plataformas.at(3)->setPos(5000,5000);


            break;
        }
        case 4:{

           uno->escenario->removeItem(uno->plataformas.at(4));
           uno->escenario->removeItem(uno->plataformas.at(5));

            uno->plataformas.at(4)->ancho=0;
            uno->plataformas.at(4)->alto=0;

            uno->plataformas.at(5)->ancho=0;
            uno->plataformas.at(5)->alto=0;

            break;
        }
        case 5:{

             uno->darItems(plataformaM5,3);
             uno->darItems(plataformaM6,3);

             uno->escenario->addItem(plataformaM5);
             uno->escenario->addItem(plataformaM6);


             break;
        }
        case 6:{
            uno->escenario->removeItem(uno->plataformas.at(6));
            uno->escenario->removeItem(uno->plataformas.at(7));

            uno->plataformas.at(6)->setPos(5000,5000);
            uno->plataformas.at(6)->alto=0;
            uno->plataformas.at(6)->ancho=0;

            uno->plataformas.at(7)->setPos(5000,5000);
            uno->plataformas.at(7)->alto=0;
            uno->plataformas.at(7)->ancho=0;


            break;
        }
        case 7:{

            uno->darItems(plataforma5,3);
            uno->darItems(plataforma6,3);

            uno->escenario->addItem(plataforma5);
            uno->escenario->addItem(plataforma6);

             uno->darItems(matriz,1);
             uno->escenario->addItem(matriz);

             uno->darItems(matriz2,1);
             uno->escenario->addItem(matriz2);

            break;
        }
        case 8:{

            uno->darItems(plataforma7,3);
            uno->darItems(plataforma8,3);

            uno->escenario->addItem(plataforma7);
            uno->escenario->addItem(plataforma8);

            break;
        }
        case 9:{

             uno->escenario->removeItem(plataformaM5);
             uno->escenario->removeItem(plataformaM6);

             plataformaM5->ancho=0;
             plataformaM5->alto=0;

             plataformaM6->ancho=0;
             plataformaM6->alto=0;

             break;
        }
        case 10:{

             uno->escenario->removeItem(plataforma5);
             uno->escenario->removeItem(plataforma6);

             uno->escenario->removeItem(matriz);
             uno->escenario->removeItem(matriz2);

             plataforma5->setPos(5000,5000);
             plataforma6->setPos(5000,5000);

             matriz->ancho=0;
             matriz->alto=0;

             matriz2->ancho=0;
             matriz2->alto=0;


             break;
        }
        case 11:{

            uno->escenario->removeItem(plataforma7);
            uno->escenario->removeItem(plataforma8);

            plataforma7->setPos(5000,5000);
            plataforma8->setPos(5000,5000);

            examen->setPos(635,629);
            uno->escenario->addItem(examen);
            TrampasMultijugador->stop();

            break;
        }
        case 12:{

        uno->darItems(plataforma9,3);
        uno->darItems(plataforma10,3);
        uno->darItems(plataforma11,3);
        uno->escenario->addItem(plataforma9);
        uno->escenario->addItem(plataforma10);
        uno->escenario->addItem(plataforma11);



        if(uno->jugador->Jvidas==1){
              plataforma9->x=168;
              plataforma9->y=589;
              plataforma9->setPos(168,589);

              plataforma10->x=288;
              plataforma10->y=505;
              plataforma10->setPos(288,505);

              plataforma11->x=448;
              plataforma11->y=320;
              plataforma11->setPos(448,250);
              plataforma11->velocidad=1;
              plataforma11->inicial=250;
              plataforma11->final=431;

        }
        if(uno->multijugador->Jvidas==1){
            plataforma9->x=1112;
            plataforma9->y=589;
            plataforma9->setPos(1112,589);

            plataforma10->x=992;
            plataforma10->y=505;
            plataforma10->setPos(992,505);

            plataforma11->x=832;
            plataforma11->y=320;
            plataforma11->setPos(832,250);
            plataforma11->velocidad=1;
            plataforma11->inicial=250;
            plataforma11->final=431;


        }
         break;
    }
        case 13:{
        if(uno->jugador->collidesWithItem(uno->plataformas.last()) ){
            uno->obstaculos.at(3)->alto=28;
            uno->obstaculos.at(3)->ancho=288;
            uno->obstaculos.at(3)->setPos(82,652);

            uno->obstaculos.at(4)->alto=28;
            uno->obstaculos.at(4)->ancho=288;
            uno->obstaculos.at(4)->setPos(372,652);

            QObject *parent=nullptr;
            items *chuzos;
            GraficoItem=new QPixmap(":/IMG/sprites/Triangulo.png");
            chuzos=new items(243,28,635,652,false,false,false,GraficoItem,parent);
            uno->darItems(chuzos,1);
            uno->escenario->addItem(chuzos);
            DesaparecerItemsMultijugador++;
            examen2->setPos(898,609);
            uno->escenario->addItem(examen2);


            Operaciones->inicial=769;
            Operaciones->final=1243;
            Operaciones->velocidad=2;
            uno->darItems(Operaciones,1);
            uno->escenario->addItem(Operaciones);



        }
        if(uno->multijugador->collidesWithItem(uno->plataformas.last()) ){
            uno->obstaculos.at(1)->alto=28;
            uno->obstaculos.at(1)->ancho=288;
            uno->obstaculos.at(1)->setPos(900,652);

            uno->obstaculos.at(2)->alto=28;
            uno->obstaculos.at(2)->ancho=288;
            uno->obstaculos.at(2)->setPos(1188,652);

            QObject *parent=nullptr;
            items *chuzos;
            GraficoItem=new QPixmap(":/IMG/sprites/Triangulo.png");
            chuzos=new items(243,28,635,652,false,false,false,GraficoItem,parent);
            uno->darItems(chuzos,1);
            uno->escenario->addItem(chuzos);
            DesaparecerItemsMultijugador++;
            examen2->setPos(382,609);
            uno->escenario->addItem(examen2);

            Operaciones->x=37;
            Operaciones->setPos(37,Operaciones->y);
            Operaciones->inicial=37;
            Operaciones->final=511;
            Operaciones->velocidad=2;
            uno->darItems(Operaciones,1);
            uno->escenario->addItem(Operaciones);



        }

        DesaparecerItemsMultijugador--;

        break;
    }
        case 14:{
        if(uno->jugador->collidesWithItem(examen2)){
            DesaparecerItemsMultijugador++;
            uno->jugador->x=829;
            uno->jugador->y=131;
            uno->jugador->setPos(uno->jugador->x,uno->jugador->y);
            examen2->setPos(1225,609);


            Operaciones2->inicial=769;
            Operaciones2->final=1243;
            Operaciones2->velocidad=2;
            uno->darItems(Operaciones2,1);
            uno->escenario->addItem(Operaciones2);

        }
        if(uno->multijugador->collidesWithItem(examen2)){
            DesaparecerItemsMultijugador++;
            uno->multijugador->x=224;
            uno->multijugador->y=131;
            uno->multijugador->setPos(uno->multijugador->x,uno->multijugador->y);
            examen2->setPos(55,609);

            Operaciones2->x=37;
            Operaciones2->setPos(37,Operaciones2->y);
            Operaciones2->inicial=37;
            Operaciones2->final=511;
            Operaciones2->velocidad=2;
            uno->darItems(Operaciones2,1);
            uno->escenario->addItem(Operaciones2);
        }

        DesaparecerItemsMultijugador--;
        break;
    }
        case 15:{
        if(uno->jugador->collidesWithItem(examen2)){
            DesaparecerItemsMultijugador++;
            uno->jugador->x=1186;
            uno->jugador->y=131;
            uno->jugador->setPos(uno->jugador->x,uno->jugador->y);


            Operaciones3->inicial=769;
            Operaciones3->final=1243;
            Operaciones3->velocidad=2;
            uno->darItems(Operaciones3,1);
            uno->escenario->addItem(Operaciones3);


            examen2->setPos(818,609);

        }
        if(uno->multijugador->collidesWithItem(examen2)){
            DesaparecerItemsMultijugador++;
            uno->multijugador->x=96;
            uno->multijugador->y=131;
            uno->multijugador->setPos(uno->multijugador->x,uno->multijugador->y);

            Operaciones3->x=37;
            Operaciones3->setPos(37,Operaciones3->y);
            Operaciones3->inicial=37;
            Operaciones3->final=511;
            Operaciones3->velocidad=2;
            uno->darItems(Operaciones3,1);
            uno->escenario->addItem(Operaciones3);

            examen2->setPos(462,609);
        }

        DesaparecerItemsMultijugador--;
        break;
    }
        case 16:{
        if(uno->jugador->collidesWithItem(examen2)){
            DesaparecerItemsMultijugador++;
            uno->jugador->x=1240;
            uno->jugador->y=131;
            uno->jugador->setPos(uno->jugador->x,uno->jugador->y);




            Operaciones4->inicial=200;
            Operaciones4->final=600;
            Operaciones4->velocidad=2;
            uno->darItems(Operaciones4,1);
            uno->escenario->addItem(Operaciones4);


            examen2->setPos(635,609);

        }
        if(uno->multijugador->collidesWithItem(examen2)){
            DesaparecerItemsMultijugador++;
            uno->multijugador->x=40;
            uno->multijugador->y=131;
            uno->multijugador->setPos(uno->multijugador->x,uno->multijugador->y);

            Operaciones4->x=504;
            Operaciones4->setPos(504,200);
            Operaciones4->inicial=200;
            Operaciones4->final=600;
            Operaciones4->velocidad=2;
            uno->darItems(Operaciones4,1);
            uno->escenario->addItem(Operaciones4);


            examen2->setPos(635,609);
        }

        DesaparecerItemsMultijugador--;
        break;
    }
        case 17:{
        if(uno->jugador->collidesWithItem(examen2)){
            DesaparecerItemsMultijugador++;
            examen2->setPos(6700,5000);

            uno->obstaculos.at(1)->alto=0;
            uno->obstaculos.at(1)->ancho=0;
            uno->obstaculos.at(1)->setPos(5000,5000);

            uno->obstaculos.at(2)->alto=0;
            uno->obstaculos.at(2)->ancho=0;
            uno->obstaculos.at(2)->setPos(5000,5000);

            uno->obstaculos.at(3)->alto=0;
            uno->obstaculos.at(3)->ancho=0;
            uno->obstaculos.at(3)->setPos(5000,5000);

            uno->obstaculos.at(4)->alto=0;
            uno->obstaculos.at(4)->ancho=0;
            uno->obstaculos.at(4)->setPos(5000,5000);

            uno->obstaculos.at(7)->alto=0;
            uno->obstaculos.at(7)->ancho=0;
            uno->obstaculos.at(7)->setPos(5000,5000);

            uno->obstaculos.at(8)->alto=0;
            uno->obstaculos.at(8)->ancho=0;
            uno->obstaculos.at(8)->x=5000;
            uno->obstaculos.at(8)->setPos(5000,0);
            uno->escenario->removeItem(uno->obstaculos.at(8));

            uno->obstaculos.at(9)->alto=0;
            uno->obstaculos.at(9)->ancho=0;
            uno->obstaculos.at(9)->x=5000;
            uno->obstaculos.at(9)->setPos(5000,0);
            uno->escenario->removeItem(uno->obstaculos.at(9));

            uno->obstaculos.at(10)->alto=0;
            uno->obstaculos.at(10)->ancho=0;
            uno->obstaculos.at(10)->x=5000;
            uno->obstaculos.at(10)->setPos(5000,0);
            uno->escenario->removeItem(uno->obstaculos.at(10));

            uno->obstaculos.at(11)->alto=0;
            uno->obstaculos.at(11)->ancho=0;
            uno->obstaculos.at(11)->x=5000;
            uno->obstaculos.at(11)->setPos(5000,0);
            uno->escenario->removeItem(uno->obstaculos.at(11));


            plataforma9->ancho=0;
            plataforma9->alto=0;
            plataforma9->setPos(5000,5000);

            plataforma10->ancho=0;
            plataforma10->alto=0;
            plataforma10->setPos(5000,5000);

            plataforma11->ancho=0;
            plataforma11->alto=0;
            plataforma11->setPos(5000,5000);
            uno->escenario->removeItem(plataforma11);

            uno->escenario->removeItem(uno->plataformas.at(uno->plataformas.length()-4));



            uno->obstaculos.at(0)->x=635;
            uno->obstaculos.at(0)->y=326;
            uno->obstaculos.at(0)->setPos(635,326);
            uno->obstaculos.at(0)->velocidad=4;
            uno->obstaculos.at(0)->inicial=326;
            uno->obstaculos.at(0)->final=644;


        }
        if(uno->multijugador->collidesWithItem(examen2)){
            DesaparecerItemsMultijugador++;
            examen2->setPos(6700,5000);
            uno->obstaculos.at(1)->alto=0;
            uno->obstaculos.at(1)->ancho=0;
            uno->obstaculos.at(1)->setPos(5000,5000);

            uno->obstaculos.at(2)->alto=0;
            uno->obstaculos.at(2)->ancho=0;
            uno->obstaculos.at(2)->setPos(5000,5000);

            uno->obstaculos.at(3)->alto=0;
            uno->obstaculos.at(3)->ancho=0;
            uno->obstaculos.at(3)->setPos(5000,5000);

            uno->obstaculos.at(4)->alto=0;
            uno->obstaculos.at(4)->ancho=0;
            uno->obstaculos.at(4)->setPos(5000,5000);

            uno->obstaculos.at(7)->alto=0;
            uno->obstaculos.at(7)->ancho=0;
            uno->obstaculos.at(7)->setPos(5000,5000);

            uno->obstaculos.at(8)->alto=0;
            uno->obstaculos.at(8)->ancho=0;
            uno->obstaculos.at(8)->x=5000;
            uno->obstaculos.at(8)->setPos(5000,0);
            uno->escenario->removeItem(uno->obstaculos.at(8));

            uno->obstaculos.at(9)->alto=0;
            uno->obstaculos.at(9)->ancho=0;
            uno->obstaculos.at(9)->x=5000;
            uno->obstaculos.at(9)->setPos(5000,0);
            uno->escenario->removeItem(uno->obstaculos.at(9));

            uno->obstaculos.at(10)->alto=0;
            uno->obstaculos.at(10)->ancho=0;
            uno->obstaculos.at(10)->x=5000;
            uno->obstaculos.at(10)->setPos(5000,0);
            uno->escenario->removeItem(uno->obstaculos.at(10));

            uno->obstaculos.at(11)->alto=0;
            uno->obstaculos.at(11)->ancho=0;
            uno->obstaculos.at(11)->x=5000;
            uno->obstaculos.at(11)->setPos(5000,0);
            uno->escenario->removeItem(uno->obstaculos.at(11));

            plataforma9->ancho=0;
            plataforma9->alto=0;
            plataforma9->setPos(5000,5000);

            plataforma10->ancho=0;
            plataforma10->alto=0;
            plataforma10->setPos(5000,5000);

            plataforma11->ancho=0;
            plataforma11->alto=0;
            plataforma11->setPos(5000,5000);
            uno->escenario->removeItem(plataforma11);

           uno->escenario->removeItem(uno->plataformas.at(uno->plataformas.length()-4));


           uno->obstaculos.at(0)->x=635;
           uno->obstaculos.at(0)->y=326;
           uno->obstaculos.at(0)->setPos(635,326);
           uno->obstaculos.at(0)->velocidad=4;
           uno->obstaculos.at(0)->inicial=326;
           uno->obstaculos.at(0)->final=644;
        }

        DesaparecerItemsMultijugador--;
        break;
    }

        default: qDebug()<<"que perro parce los mato";
    }


}


// Juego ) ----------------------------------------------------------------------------------------
void MainWindow::Juego(QString datosdejugador){


   QString ObtenerNombre,ObtenerTiempo;
   DatosJugadorAuxiliar=datosdejugador;
   int ObtenerVidas,NivelActual;


   int n;
   //Obtener tiempo actual de jugador.
   n=datosdejugador.indexOf(" ");
   ObtenerTiempo=datosdejugador.left(n);
   datosdejugador.remove(0,n+1);

   //obtiene nombre de usuario
   n=datosdejugador.indexOf(" ");
   ObtenerNombre=datosdejugador.left(n);
   datosdejugador.remove(0,n+1);

   //Borrando contraseña
   n=datosdejugador.indexOf(" ");
   datosdejugador.remove(0,n+1);

   //obtener nivel actual
   n=datosdejugador.indexOf(" ");
   NivelActual=(datosdejugador.left(n)).toInt();
   Nivelito=NivelActual;
   datosdejugador.remove(0,n+1);

   //Obtener vidas
   n=datosdejugador.indexOf(" ");
   ObtenerVidas=(datosdejugador.left(n)).toInt();
   datosdejugador.remove(0,n+1);

   if(Nivelito==3){

        findeljuego=new QGraphicsScene(0,0,1280,720);
        ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/Gracias.jpg"));
        ui->graphicsView->setScene(findeljuego);


        regresar=new QPushButton;
        regresar->setGeometry(640,550,150,30);

         QPixmap flechita(":/IMG/sprites/Flecha.png");
         QIcon ButtonIcon(flechita);
         regresar->setIcon(ButtonIcon);
         regresar->setIconSize(flechita.rect().size());
         regresar->adjustSize();
         regresar->setStyleSheet("QPushButton { background-color : transparent; border : none; }");
         findeljuego->addWidget(regresar);
         connect(regresar,SIGNAL (released()),this,SLOT (devolverse()));
   }
   else{
       QObject *parent = nullptr;
       Principal=new Personajes(ObtenerVidas,ObtenerNombre,parent);

       // Se crean todos los elementos del nivel item, cronometro, jugador

       serial.setPortName("COM8");
       if(serial.open(QIODevice::ReadWrite)){
           if(!serial.setBaudRate(QSerialPort::Baud9600))
               qDebug()<<serial.errorString();
           if(!serial.setDataBits(QSerialPort::Data8))
               qDebug()<<serial.errorString();
           if(!serial.setParity(QSerialPort::NoParity))
               qDebug()<<serial.errorString();
           if(!serial.setStopBits(QSerialPort::OneStop))
               qDebug()<<serial.errorString();
           if(!serial.setFlowControl(QSerialPort::NoFlowControl))
               qDebug()<<serial.errorString();
           arduino=true;
       }else{
           arduino=false;
           qDebug()<<"Conecta Tu Arduino Para Activar Bonus Random";
       }

       Arduino=new QTimer;
       Arduino->start(750);
       connect(Arduino,SIGNAL (timeout()),SLOT (arduidos()));



       //GENERAL PARA TODO NIVEL
       musica->stop();
       MusicaNivel->play();



       colision=new QTimer;
       colision->stop();
       colision->start(5);
       connect(colision,SIGNAL(timeout()),SLOT(Colisiones()));

       Principal->Jnivel=NivelActual;

       //Creando los QLCDNumber que actuan como cronometros.
       if(NivelActual==1)uno=new nivel(69,188,1218,665,Principal);
       else if(NivelActual==2)uno=new nivel(66,642,1221,223,Principal);
       BloquearTeclado=true;
       Minutos=new QLCDNumber;
       Segundos=new QLCDNumber;
       Milesimas=new QLCDNumber;

       Minutos->setGeometry(929,1,80,80);
       Segundos->setGeometry(1007,1,80,80);
       Milesimas->setGeometry(1084,1,80,80);

       Minutos->setStyleSheet("QLCDNumber { background-color : transparent; color : black; }");
       Segundos->setStyleSheet("QLCDNumber { background-color : transparent; color : black; }");
       Milesimas->setStyleSheet("QLCDNumber { background-color : transparent; color : black; }");

       Minutos->display((ObtenerTiempo.left(2)).toDouble());
       ObtenerTiempo.remove(0,3);
       Segundos->display(ObtenerTiempo.left(2).toDouble());
       ObtenerTiempo.remove(0,3);
       Milesimas->display(ObtenerTiempo.left(2).toDouble());

       uno->escenario->addWidget(Minutos);
       uno->escenario->addWidget(Segundos);
       uno->escenario->addWidget(Milesimas);

       cronometro=new QTimer;
       cronometro->stop();
       cronometro->start(1000);
       connect(cronometro,SIGNAL(timeout()),SLOT(actualizarTiempo()));
       ui->graphicsView->setGeometry(0,0,1280,720);


       ui->graphicsView->setScene(uno->escenario);
       ui->graphicsView->show();



       if(NivelActual==1){ // Nivel 1

           ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/FondoNivelUno.png"));
           //1.Obstaculos
           //2.Beneficios
           //3.Plataformas
           GraficoItem=new QPixmap(":/IMG/sprites/Cuadro.png");
           items *Bloque;
           Bloque=new items(206,46,300,90,true,true,true,GraficoItem,parent);
           Bloque->inicial=90;
           Bloque->final=644;
           Bloque->velocidad=2;
           uno->darItems(Bloque,1);


           items *chuzos;
           GraficoItem=new QPixmap(":/IMG/sprites/Triangulo.png");
           chuzos=new items(288,28,800,652,false,false,false,GraficoItem,parent);
           uno->darItems(chuzos,1);



           items *plataforma;
           GraficoItem=new QPixmap(":/IMG/sprites/PlataformaRegla.png");
           plataforma=new items(70,23,700,620,false,false,false,GraficoItem,parent);
           uno->darItems(plataforma,3);



           items *plataformaM;
           plataformaM=new items(70,23,1000,580,true,false,true,GraficoItem,parent);
           plataformaM->inicial=850;
           plataformaM->final=1000;
           uno->darItems(plataformaM,3);



           items *examen;
           GraficoItem=new QPixmap(":/IMG/sprites/ItemExamen.png");
           examen=new items(20,30,780,470,false,false,false,GraficoItem,parent);
           examen->tipo=1;
           uno->darItems(examen,2);



           items *checkpoint;
           GraficoItem=new QPixmap(":/IMG/sprites/LapizChekpoint2.png");
           checkpoint=new items(12,98,500,618,false,false,false,GraficoItem,parent);
           checkpoint->tipo=5;
           uno->darItems(checkpoint,2);



           items *devolver;
           GraficoItem=new QPixmap(":/IMG/sprites/ItemExamenM.png");
           devolver=new items(20,30,1100,620,false,false,false,GraficoItem,parent);
           devolver->tipo=9;
           uno->darItems(devolver,2);

           items *Pendulo2;
           GraficoItem=new QPixmap(":/IMG/sprites/Esfera.png");
           Pendulo2=new items(50,50,1100,500,false,false,false,GraficoItem,parent);
           Pendulo2->devuelve=true;
           uno->darItems(Pendulo2,1);

           Pendulo=new QTimer;
           connect(Pendulo,SIGNAL (timeout()),SLOT(pendulo()));
           Pendulo->start(10);

           Cuerda=new QGraphicsLineItem;
           uno->escenario->addItem(Cuerda);

           QGraphicsEllipseItem *Puntilla=new QGraphicsEllipseItem(1170,495,10,10);
           Puntilla->setBrush(Qt::black);
           uno->escenario->addItem(Puntilla);





           uno->ActualizarItems();
       }else{
           if(NivelActual==2){
               ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/FondoNivelDos.jpg"));
               uno->puerta->setLine(1194,223,1241,223);



               Disparo=new QTimer;
               connect(Disparo,SIGNAL (timeout()),SLOT (disparando()));

               GraficoItem=new QPixmap(":/IMG/sprites/Matriz.png");
               items *Esfera2=new items(70,65,883,150,false,false,false,GraficoItem,parent);
               Esfera2->inicial=150;
               Esfera2->final=360;
               Esfera2->Ax=0;
               Esfera2->Vx=0;
               Esfera2->esfera=true;
               uno->darItems(Esfera2,1);




               items *plataforma;
               GraficoItem=new QPixmap(":/IMG/sprites/PlataformaRegla.png");
               plataforma=new items(70,23,606,603,false,false,false,GraficoItem,parent);
               uno->darItems(plataforma,3);

               items *plataforma0;
               plataforma0=new items(70,23,606,520,false,false,false,GraficoItem,parent);
               uno->darItems(plataforma0,3);


               items *plataforma2;
               plataforma2=new items(70,23,883,450,false,false,false,GraficoItem,parent);
               uno->darItems(plataforma2,3);


               items *plataforma3;
               plataforma3=new items(70,23,400,450,false,false,false,GraficoItem,parent);
               uno->darItems(plataforma3,3);


               items *plataforma4;
               plataforma4=new items(70,23,180,422,false,false,false,GraficoItem,parent);
               uno->darItems(plataforma4,3);


               items *plataforma5;
               plataforma5=new items(40,23,1220,237,false,false,false,GraficoItem,parent);
               uno->darItems(plataforma5,3);


               items *plataformaM5;
               plataformaM5=new items(70,23,1058,422,true,true,true,GraficoItem,parent);
               plataformaM5->inicial=150;
               plataformaM5->final=422;
               uno->darItems(plataformaM5,3);


               GraficoItem=new QPixmap(":/IMG/sprites/Limite.png");
               Limite=new items(70,100,1220,187,false,false,false,GraficoItem,parent);
               uno->darItems(Limite,1);



               QPixmap libros(":/IMG/sprites/Libros.png");
               QGraphicsPixmapItem *Libros=new QGraphicsPixmapItem;
               Libros->setPixmap(libros);
               Libros->setPos(1220,582);
               uno->escenario->addItem(Libros);


               QPixmap *examenSwitch=new QPixmap(":/IMG/sprites/ItemExamen.png");
               ExamenSwitch=new QGraphicsPixmapItem;
               ExamenSwitch->setPixmap(*examenSwitch);
               ExamenSwitch->setPos(1260,644);

               ExamenSwitch2=new QGraphicsPixmapItem;
               ExamenSwitch2->setPixmap(*examenSwitch);
               ExamenSwitch2->setPos(875,410);
               uno->escenario->addItem(ExamenSwitch2);




               items *Examen1=new items(20,30,50,275,false,false,false,examenSwitch,parent);
               Examen1->tipo=11;
               uno->darItems(Examen1,2);


               MaquinaVectores=new QTimer;
               connect(MaquinaVectores,SIGNAL (timeout()),SLOT (maquinavectores()));
               MaquinaVectores->start(1000);

               Esfera=new QTimer;
               connect(Esfera,SIGNAL(timeout()),SLOT(esferando()));


               uno->ActualizarItems();


               uno->escenario->removeItem(uno->plataformas.at(5));


           }
       }
   }
}
void MainWindow::pendulo(){


    Cuerda->setLine(1175,500,uno->obstaculos.last()->x,uno->obstaculos.last()->y);

    if(uno->obstaculos.last()->x>=1100 && uno->obstaculos.last()->devuelve){
        if(uno->obstaculos.last()->x<=1175){


            if(uno->obstaculos.last()->x<=1138){
                uno->obstaculos.last()->x++;
                uno->obstaculos.last()->y+=2;
                uno->obstaculos.last()->setPos(uno->obstaculos.last()->x,uno->obstaculos.last()->y);
            }else{
                uno->obstaculos.last()->x+=2;
                uno->obstaculos.last()->y++;
                uno->obstaculos.last()->setPos(uno->obstaculos.last()->x,uno->obstaculos.last()->y);
            }


        }else{
            if(uno->obstaculos.last()->x<=1212){
                uno->obstaculos.last()->x+=2;
                uno->obstaculos.last()->y--;
                uno->obstaculos.last()->setPos(uno->obstaculos.last()->x,uno->obstaculos.last()->y);
            }else{
                uno->obstaculos.last()->x++;
                uno->obstaculos.last()->y-=2;
                uno->obstaculos.last()->setPos(uno->obstaculos.last()->x,uno->obstaculos.last()->y);
            }
        }
        if( uno->obstaculos.last()->x==1250) uno->obstaculos.last()->devuelve=false;
    }




    if(uno->obstaculos.last()->x>=1100 && !uno->obstaculos.last()->devuelve){
        if(uno->obstaculos.last()->x<=1175){


            if(uno->obstaculos.last()->x<=1138){
                uno->obstaculos.last()->x--;
                uno->obstaculos.last()->y-=2;
                uno->obstaculos.last()->setPos(uno->obstaculos.last()->x,uno->obstaculos.last()->y);
            }else{
                uno->obstaculos.last()->x-=2;
                uno->obstaculos.last()->y--;
                uno->obstaculos.last()->setPos(uno->obstaculos.last()->x,uno->obstaculos.last()->y);
            }


        }else{
            if(uno->obstaculos.last()->x<=1212){
                uno->obstaculos.last()->x-=2;
                uno->obstaculos.last()->y++;
                uno->obstaculos.last()->setPos(uno->obstaculos.last()->x,uno->obstaculos.last()->y);
            }else{
                uno->obstaculos.last()->x--;
                uno->obstaculos.last()->y+=2;
                uno->obstaculos.last()->setPos(uno->obstaculos.last()->x,uno->obstaculos.last()->y);
            }
        }
        if( uno->obstaculos.last()->x==1100) uno->obstaculos.last()->devuelve=true;
    }
}
void MainWindow::esferando(){
   if(!uno->obstaculos.at(0)->collidesWithItem(uno->plataformas.at(2)) && uno->obstaculos.at(0)->y<400) {
        double r2=pow(25,2);
        double v2=pow(uno->obstaculos.last()->Vx,2);
        int ang=90;
        uno->obstaculos.at(0)->Ax= -1*v2*r2*double(sin(ang))/20 -10.0;
         uno->obstaculos.at(0)->Vx+=uno->obstaculos.at(0)->Ax*0.03;
         uno->obstaculos.at(0)->y+=-1*(uno->obstaculos.at(0)->Vx*0.03+uno->obstaculos.at(0)->Ax*pow(0.03,2)/2);
         uno->obstaculos.at(0)->setPos(uno->obstaculos.at(0)->x,uno->obstaculos.at(0)->y);

    }
   if(uno->obstaculos.at(0)->collidesWithItem(uno->plataformas.at(2))){
       uno->obstaculos.at(0)->y= uno->obstaculos.at(0)->y+1;
       uno->obstaculos.at(0)->Vx=0;
   }


}
void MainWindow:: maquinavectores(){
    QObject *parent=nullptr;
    QPixmap *vector=new QPixmap(":/IMG/sprites/Vector.png");
    items *Vector =new items(46,6,1260,660,false,false,false,vector,parent);
    uno->darItems(Vector,1);
    uno->escenario->addItem(Vector);

    Disparo->start(1);
    MaquinaVectores->stop();


}

void MainWindow::disparando()
{
    uno->obstaculos.last()->Ax=1;
    uno->obstaculos.last()->Vx=5;

    if(uno->obstaculos.last()->x>-50){
        uno->obstaculos.last()->x-=uno->obstaculos.last()->Vx-uno->obstaculos.last()->Ax/2;
        uno->obstaculos.last()->setPos(uno->obstaculos.last()->x,660);
        uno->obstaculos.last()->Vx+=uno->obstaculos.last()->Ax;
        uno->obstaculos.last()->Ax++;
        if(uno->jugador->collidesWithItem(uno->obstaculos.last())){
            uno->obstaculos.last()->x=-500;
            uno->obstaculos.last()->setPos(-500,660);
            uno->jugador->doblesalto=0;
            uno->jugador->Jvidas-=1;
            uno->vidas--;
            uno->Datos->setText(uno->jugador->Jnombre+" : "+QString::number(uno->vidas));
            uno->jugador->x=uno->Px1;
            uno->jugador->y=uno->Py1;
            uno->jugador->setPos(uno->Px1,uno->Py1);
        }
    }else{
        Disparo->stop();
        MaquinaVectores->start(1000);
    }
}


// Dato ) -----------------------------------------------------------------------------------------
int MainWindow::Dato(QString L){
    for(int i=0;i<3;i++){
        L.remove(0,L.indexOf(" ")+1);
    }
    return L.left(L.indexOf(" ")).toInt();


}


// Actualizar tiempo ) ----------------------------------------------------------------------------
void MainWindow::actualizarTiempo(){// Actualiza los cronometros cada centesima de segundo
    double m,s,ms;
    m=Minutos->value();
    s=Segundos->value();
    ms=Milesimas->value();

    if(ms<99)Milesimas->display(ms+1);
    else{
        Milesimas->display(0);
        if(s<59)Segundos->display(s+1);
        else{Segundos->display(0);
            Minutos->display(m+1);
        }
    }
    cronometro->start(10);
}


// Colisiones ) -----------------------------------------------------------------------------------
void MainWindow::Colisiones(){
    if(uno->jugador->collidesWithItem(uno->derecha))uno->jugador->Derecha=true;
    else uno->jugador->Derecha=false;
    if(uno->jugador->collidesWithItem(uno->izquierda))uno->jugador->Izquierda=true;
    else uno->jugador->Izquierda=false;
    if(Nivelito==2 && uno->jugador->collidesWithItem(ExamenSwitch)){
        ExamenSwitch->setPos(6500,6500);
        Limite->setPos(6550,6550);
    }
    if(Nivelito==2 && uno->jugador->collidesWithItem(ExamenSwitch2)){
        ExamenSwitch2->setPos(6500,6500);
        Esfera->start(10);
        uno->jugador->ActivarMovimientos=false;
    }


    if(uno->SonDos){
        if(uno->multijugador->collidesWithItem(uno->derecha)){uno->multijugador->Derecha=true;uno->multijugador->MDerecha=false;}
        else uno->multijugador->Derecha=false;
        if(uno->multijugador->collidesWithItem(uno->izquierda)){uno->multijugador->Izquierda=true;uno->multijugador->MIzquierda=false;}
        else uno->multijugador->Izquierda=false;

        if(uno->jugador->collidesWithItem(examen)){

           uno->puerta->setLine(1175,664,1222,664);

           TrampasMultijugador->start(5);
            examen->setPos(10000,10000);
            uno->obstaculos.at(3)->alto=0;
            uno->obstaculos.at(3)->ancho=0;
            uno->obstaculos.at(3)->setPos(5000,5000);

            uno->obstaculos.at(4)->alto=0;
            uno->obstaculos.at(4)->ancho=0;
            uno->obstaculos.at(4)->setPos(5000,5000);

            uno->jugador->x=uno->Px1;
            uno->jugador->setPos(uno->Px1,uno->jugador->y);

            if(uno->multijugador->Jvidas==1){

                uno->multijugador->x=5000;
                uno->multijugador->setPos(5000,5000);
                uno->Datos2->setText("Eliminado.");
            }
            uno->obstaculos.at(0)->x=5800;
            uno->obstaculos.at(0)->y=3633;
            uno->obstaculos.at(0)->setPos(5800,3633);

            QObject *parent=nullptr;
            items *Bloque;
            GraficoItem=new QPixmap(":/IMG/sprites/Cuadro.png");
            Bloque=new items(206,46,635,326,false,false,false,GraficoItem,parent);
            uno->darItems(Bloque,3);
            uno->escenario->addItem(Bloque);
       }

        if(uno->multijugador->collidesWithItem(examen)){

            uno->puerta->setLine(43,664,92,664);

            TrampasMultijugador->start(5);
            examen->setPos(10000,10000);
            uno->obstaculos.at(1)->alto=0;
            uno->obstaculos.at(1)->ancho=0;
            uno->obstaculos.at(1)->setPos(5000,5000);

            uno->obstaculos.at(2)->alto=0;
            uno->obstaculos.at(2)->ancho=0;
            uno->obstaculos.at(2)->setPos(5000,5000);

            uno->multijugador->x=uno->Px3;
            uno->multijugador->setPos(uno->Px3,uno->multijugador->y);

            if(uno->jugador->Jvidas==1){

                uno->jugador->x=5000;
                uno->jugador->setPos(5000,5000);
                uno->Datos->setText("Eliminado.");
            }

            uno->obstaculos.at(0)->x=5800;
            uno->obstaculos.at(0)->y=3633;
            uno->obstaculos.at(0)->setPos(5800,3633);

            QObject *parent=nullptr;
            items *Bloque;
            GraficoItem=new QPixmap(":/IMG/sprites/Cuadro.png");
            Bloque=new items(206,46,635,326,false,false,false,GraficoItem,parent);
            uno->darItems(Bloque,3);
            uno->escenario->addItem(Bloque);
       }


    }

    if(!uno->SonDos)
    {
        if(uno->jugador->Jvidas<=0){

            colision->stop();
            MusicaNivel->stop();

            BloquearTeclado=false;

            QFile leer(RUTA_ARCHIVO);
            leer.open(QIODevice::ReadOnly);
            QString ContenidoTxT=leer.readAll();
            leer.close();

            QFile escribir(RUTA_ARCHIVO);
            escribir.open(QIODevice::WriteOnly);

            QString ContenidoFinalTxT="";


            while(ContenidoTxT.length()>0){
                QString lineasTxT=ContenidoTxT.left(ContenidoTxT.indexOf("\n"));
                ContenidoTxT.remove(0,ContenidoTxT.indexOf("\n")+1);
                if(!(lineasTxT.right(lineasTxT.length()-9).startsWith(uno->jugador->Jnombre+" "+letrero4->text())))ContenidoFinalTxT+=lineasTxT+"\n";
            }

            QTextStream stream( &escribir );
            stream <<ContenidoFinalTxT;
            escribir.close();


            cronometro->stop();

            uno->Obstaculos->stop();
            uno->Plataformas->stop();
            uno->Bonificaciones->stop();

            uno->jugador->Caer->stop();
            uno->jugador->Salto->stop();

            for(int i=0;i<uno->obstaculos.size();i++){uno->obstaculos.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->plataformas.size();i++){uno->plataformas.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->bonificaciones.size();i++){uno->bonificaciones.at(i)->MovimientoItem->stop();}

            if(Nivelito)Pendulo->stop();
            if(Nivelito==2){
                MaquinaVectores->stop();
                Disparo->stop();
                Esfera->stop();
            }

            findeljuego=new QGraphicsScene(0,0,1280,720);
            ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/FindelJUegoUnjugador.jpg"));
            ui->graphicsView->setScene(findeljuego);

            QLabel *Perdio=new QLabel();
            Perdio->setStyleSheet("QLabel { background-color : transparent; color : White; }");
            switch (Nivelito) {
            case 1:{
                Perdio->setText("No supo escribir su nombre.");
                Perdio->setFont(QFont("Ebrima",30));
                findeljuego->addWidget(Perdio);
                Perdio->setGeometry(550,535,90,20);
                Perdio->adjustSize();
                break;
            }

            default:  Perdio->setText("Era muy feo.");
                Perdio->setFont(QFont("Ebrima",30));
                findeljuego->addWidget(Perdio);
                Perdio->setGeometry(550,535,90,20);
                Perdio->adjustSize();
                break;
            }



            regresar=new QPushButton;
            regresar->setGeometry(180,320,150,30);

            QPixmap flechita(":/IMG/sprites/Flecha.png");
            QIcon ButtonIcon(flechita);
            regresar->setIcon(ButtonIcon);
            regresar->setIconSize(flechita.rect().size());
            regresar->adjustSize();
            regresar->setStyleSheet("QPushButton { background-color : transparent; border : none; }");
            findeljuego->addWidget(regresar);
            connect(regresar,SIGNAL (released()),this,SLOT (devolverse()));


        }

        if(uno->jugador->collidesWithItem(uno->puerta)){
            uno->jugador->voltear();
            MusicaNivel->stop();

            cronometro->stop();
            QString tiempo="";
            if(int(Minutos->value())/10==0)tiempo+="0"+QString::number(Minutos->value())+":";
            else tiempo+=QString::number(Minutos->value())+":";
            if(int(Segundos->value())/10==0)tiempo+="0"+  QString::number(Segundos->value())+".";
            else tiempo+=QString::number(Segundos->value())+".";
            if(int(Milesimas->value())/10==0)tiempo+="0"+  QString::number(Milesimas->value());
            else tiempo+=QString::number(Milesimas->value());

            DatosJugadorAuxiliar.remove(0,9);
            DatosJugadorAuxiliar.remove(DatosJugadorAuxiliar.left(DatosJugadorAuxiliar.indexOf(" ")+1));
            QString codigo=letrero4->text();
            DatosJugadorAuxiliar=tiempo+" "+uno->jugador->Jnombre+" "+codigo+" "+QString::number(uno->jugador->Jnivel+1)+" "+QString::number(uno->jugador->Jvidas);
            uno->jugador->Jnivel++;
            QFile leer(RUTA_ARCHIVO);
            leer.open(QIODevice::ReadOnly);
            QString ContenidoTxT=leer.readAll();
            leer.close();
            QFile escribir(RUTA_ARCHIVO);
            escribir.open(QIODevice::WriteOnly);
            QString ContenidoFinalTxT="";




            if(Nivelito==1)Pendulo->stop();


            if(uno->jugador->Jnivel<3){
                while(ContenidoTxT.length()>0){
                    QString lineasTxT=ContenidoTxT.left(ContenidoTxT.indexOf("\n"));
                    ContenidoTxT.remove(0,ContenidoTxT.indexOf("\n")+1);
                    if(lineasTxT.right(lineasTxT.length()-9).startsWith(uno->jugador->Jnombre+" "+codigo))ContenidoFinalTxT+=DatosJugadorAuxiliar;
                    else ContenidoFinalTxT+=lineasTxT;
                    ContenidoFinalTxT+='\n';
                }
            }else{
                while(ContenidoTxT.length()>0){//borra usuario de la lista
                    QString lineasTxT=ContenidoTxT.left(ContenidoTxT.indexOf("\n"));
                    ContenidoTxT.remove(0,ContenidoTxT.indexOf("\n")+1);
                    if(!(lineasTxT.right(lineasTxT.length()-9).startsWith(uno->jugador->Jnombre+" "+codigo)))ContenidoFinalTxT+=lineasTxT+"\n";
                }
                ContenidoTxT=ContenidoFinalTxT;
                ContenidoFinalTxT="";
                QString auxiliarJugador=DatosJugadorAuxiliar;



                while(ContenidoTxT.length()>0){
                    QString lineasTxT=ContenidoTxT.left(ContenidoTxT.indexOf("\n"));
                    DatosJugadorAuxiliar=auxiliarJugador;
                    QString auxiliarLinea=lineasTxT;
                    ContenidoTxT.remove(0,ContenidoTxT.indexOf("\n")+1);

                    //----------------------------------------------------------------------------------------------
                    if(Dato(lineasTxT)==3){

                        if(DatosJugadorAuxiliar.left(2).toInt()>lineasTxT.left(2).toInt()){
                            ContenidoFinalTxT+=auxiliarLinea+"\n";
                        }else{
                            if(DatosJugadorAuxiliar.left(2).toInt()<lineasTxT.left(2).toInt()){

                                ContenidoFinalTxT+=auxiliarJugador+"\n";
                                ContenidoFinalTxT+=auxiliarLinea+"\n";
                                ContenidoFinalTxT+=ContenidoTxT;
                                break;
                            }else{
                                DatosJugadorAuxiliar.remove(0,3);
                                lineasTxT.remove(0,3);
                                if(DatosJugadorAuxiliar.left(2).toInt()>lineasTxT.left(2).toInt()){
                                    ContenidoFinalTxT+=auxiliarLinea+"\n";
                                }else{
                                    if(DatosJugadorAuxiliar.left(2).toInt()<lineasTxT.left(2).toInt()){
                                        ContenidoFinalTxT+=auxiliarJugador+"\n";
                                        ContenidoFinalTxT+=auxiliarLinea+"\n";
                                        ContenidoFinalTxT+=ContenidoTxT;
                                        break;
                                    }else{
                                        DatosJugadorAuxiliar.remove(0,3);
                                        lineasTxT.remove(0,3);
                                        if(DatosJugadorAuxiliar.left(2).toInt()>lineasTxT.left(2).toInt()){
                                            ContenidoFinalTxT+=auxiliarLinea+"\n";
                                        }else{
                                            if(DatosJugadorAuxiliar.left(2).toInt()<lineasTxT.left(2).toInt()){
                                                ContenidoFinalTxT+=auxiliarJugador+"\n";
                                                ContenidoFinalTxT+=auxiliarLinea+"\n";
                                                ContenidoFinalTxT+=ContenidoTxT;
                                                break;
                                            }else{
                                                ContenidoFinalTxT+=auxiliarJugador+"\n";
                                                ContenidoFinalTxT+=auxiliarLinea+"\n";
                                                ContenidoFinalTxT+=ContenidoTxT;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else{//puede que DatosJugadorAuxiliar se escriba siempre(despues de encontrar su posicion)
                        ContenidoFinalTxT+=auxiliarJugador+"\n";
                        ContenidoFinalTxT+=auxiliarLinea+"\n";
                        ContenidoFinalTxT+=ContenidoTxT;
                        break;
                    }
                }

            }
            QTextStream stream( &escribir );
            stream <<ContenidoFinalTxT;
            escribir.close();

            colision->stop();
            cronometro->stop();

            uno->Obstaculos->stop();
            uno->Plataformas->stop();
            uno->Bonificaciones->stop();

            uno->jugador->Caer->stop();
            uno->jugador->Salto->stop();

            for(int i=0;i<uno->obstaculos.size();i++){uno->obstaculos.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->plataformas.size();i++){uno->plataformas.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->bonificaciones.size();i++){uno->bonificaciones.at(i)->MovimientoItem->stop();}

            if(Nivelito==2){
                MaquinaVectores->stop();
                Disparo->stop();
                Esfera->stop();
            }

            Juego(DatosJugadorAuxiliar);

        }


    }else{
        if(uno->jugador->Jvidas<=0){
            uno->jugador->x=5000;
            uno->jugador->setPos(5000,5000);
            uno->Datos->setText("Eliminado.");
            bandera=false;
        }
        if(uno->multijugador->Jvidas<=0){
            uno->multijugador->x=5000;
            uno->multijugador->setPos(5000,5000);
            uno->Datos2->setText("Eliminado.");
            bandera2=false;
        }

        if(uno->jugador->Jvidas<=0 && uno->multijugador->Jvidas<=0){


            colision->stop();
            MusicaNivel->stop();

            BloquearTeclado=false;

            TrampasMultijugador->stop();
            uno->Obstaculos->stop();
            uno->Plataformas->stop();
            uno->Bonificaciones->stop();

            uno->jugador->Caer->stop();
            uno->multijugador->Caer->stop();

            uno->jugador->Salto->stop();
            uno->multijugador->Salto->stop();

            for(int i=0;i<uno->obstaculos.size();i++){uno->obstaculos.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->bonificaciones.size();i++){uno->bonificaciones.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->plataformas.size();i++){uno->plataformas.at(i)->MovimientoItem->stop();}


            DesaparecerItemsMultijugador=0;


            findeljuego=new QGraphicsScene(0,0,1280,720);
            ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/FindelJuegoMultijugador.jpg"));
            ui->graphicsView->setScene(findeljuego);

           regresar=new QPushButton;
           regresar->setGeometry(180,320,150,30);

            QPixmap flechita(":/IMG/sprites/Flecha.png");
            QIcon ButtonIcon(flechita);
            regresar->setIcon(ButtonIcon);
            regresar->setIconSize(flechita.rect().size());
            regresar->adjustSize();
            regresar->setStyleSheet("QPushButton { background-color : transparent; border : none; }");
            findeljuego->addWidget(regresar);
            connect(regresar,SIGNAL (released()),this,SLOT (devolverse()));




        }

        if(uno->jugador->collidesWithItem(uno->puerta)){
            uno->jugador->voltear();

            MusicaNivel->stop();

            colision->stop();

             BloquearTeclado=false;

            TrampasMultijugador->stop();
            uno->Obstaculos->stop();
            uno->Plataformas->stop();
            uno->Bonificaciones->stop();

            uno->jugador->Caer->stop();
            uno->multijugador->Caer->stop();

            uno->jugador->Salto->stop();
            uno->multijugador->Salto->stop();

            for(int i=0;i<uno->obstaculos.size();i++){uno->obstaculos.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->bonificaciones.size();i++){uno->bonificaciones.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->plataformas.size();i++){uno->plataformas.at(i)->MovimientoItem->stop();}


            DesaparecerItemsMultijugador=0;


            findeljuego=new QGraphicsScene(0,0,1280,720);
            ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/GanaJugadorUno.jpg"));
            ui->graphicsView->setScene(findeljuego);

           regresar=new QPushButton;
           regresar->setGeometry(180,320,150,30);

            QPixmap flechita(":/IMG/sprites/Flecha.png");
            QIcon ButtonIcon(flechita);
            regresar->setIcon(ButtonIcon);
            regresar->setIconSize(flechita.rect().size());
            regresar->adjustSize();
            regresar->setStyleSheet("QPushButton { background-color : transparent; border : none; }");
            findeljuego->addWidget(regresar);
            connect(regresar,SIGNAL (released()),this,SLOT (devolverse()));



        }

        if(uno->multijugador->collidesWithItem(uno->puerta)){
            uno->multijugador->voltear();

            MusicaNivel->stop();

            colision->stop();

             BloquearTeclado=false;

            TrampasMultijugador->stop();
            uno->Obstaculos->stop();
            uno->Plataformas->stop();
            uno->Bonificaciones->stop();

            uno->jugador->Caer->stop();
            uno->multijugador->Caer->stop();

            uno->jugador->Salto->stop();
            uno->multijugador->Salto->stop();

            for(int i=0;i<uno->obstaculos.size();i++){uno->obstaculos.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->bonificaciones.size();i++){uno->bonificaciones.at(i)->MovimientoItem->stop();}
            for(int i=0;i<uno->plataformas.size();i++){uno->plataformas.at(i)->MovimientoItem->stop();}


            DesaparecerItemsMultijugador=0;


            findeljuego=new QGraphicsScene(0,0,1280,720);
            ui->graphicsView->setBackgroundBrush(QImage(":/IMG/sprites/GanaJUgadorDos.jpg"));
            ui->graphicsView->setScene(findeljuego);

           regresar=new QPushButton;
           regresar->setGeometry(180,320,150,30);

            QPixmap flechita(":/IMG/sprites/Flecha.png");
            QIcon ButtonIcon(flechita);
            regresar->setIcon(ButtonIcon);
            regresar->setIconSize(flechita.rect().size());
            regresar->adjustSize();
            regresar->setStyleSheet("QPushButton { background-color : transparent; border : none; }");
            findeljuego->addWidget(regresar);
            connect(regresar,SIGNAL (released()),this,SLOT (devolverse()));


        }
    }
}


// arduidos ) -------------------------------------------------------------------------------------
void MainWindow::arduidos(){
    if(arduino){

        if(!serial.waitForReadyRead(150) && bonus>0){
            Arduino->stop();
            bonus--;
            int x=100+rand()%(300-100+1);
            switch (x/100){
            case 1:uno->jugador->Jvidas-=4;uno->vidas-=4;uno->Datos->setText(uno->jugador->Jnombre+" : "+QString::number(uno->vidas));break;
            case 2:uno->jugador->x=uno->Px2-100;uno->jugador->setPos(uno->jugador->x,uno->jugador->y);break;
            case 3:uno->jugador->Jvidas+=2;uno->vidas+=2;uno->Datos->setText(uno->jugador->Jnombre+" : "+QString::number(uno->vidas));break;
            default: for(int i=0;i<uno->obstaculos.size();i++){if(uno->obstaculos.at(i)->Movibles && uno->obstaculos.at(i)->vertical && uno->obstaculos.at(i)->devuelve){uno->jugador->x=uno->obstaculos.at(i)->x;uno->jugador->y=644;uno->jugador->setPos(uno->jugador->x,644);break;}};;
            }
            Arduino->start(750);
        }
    }
}


// Eventos de teclado ) ----------------------------------------------------------------------------
void MainWindow::keyPressEvent(QKeyEvent * evento){
    if(evento->key()==Qt::Key_1)exit(1);
    if(BloquearTeclado){
        if(evento->key()==Qt::Key_L && !uno->jugador->Segundo && bandera==true){uno->jugador->x-=uno->friccion;uno->jugador->MDerecha=true;uno->jugador->moviendose=true;}
        if(evento->key()==Qt::Key_J && !uno->jugador->Segundo && bandera==true){uno->jugador->x+=uno->friccion;uno->jugador->MIzquierda=true;uno->jugador->moviendose=true;}
        if(evento->key()==Qt::Key_I && !uno->jugador->Segundo && bandera==true){SaltoSonido->stop();uno->jugador->MArriba=true;uno->jugador->moviendose=true;SaltoSonido->play();}
        if(evento->key()==Qt::Key_K && !uno->jugador->Segundo && bandera==true){uno->jugador->MAbajo=true;uno->jugador->moviendose=true;}
        if(uno->SonDos){
            if(evento->key()==Qt::Key_D && !uno->multijugador->Derecha && uno->multijugador->Segundo && bandera2==true){uno->multijugador->x-=uno->friccion;uno->multijugador->MDerecha=true;uno->multijugador->moviendose=true;}
            if(evento->key()==Qt::Key_A && !uno->multijugador->Izquierda && uno->multijugador->Segundo && bandera2==true){uno->multijugador->x+=uno->friccion;uno->multijugador->MIzquierda=true;uno->multijugador->moviendose=true;}
            if(evento->key()==Qt::Key_W && uno->multijugador->Segundo && bandera2==true){SaltoSonido->stop();uno->multijugador->MArriba=true;uno->multijugador->moviendose=true;SaltoSonido->play();}
            if(evento->key()==Qt::Key_S && uno->multijugador->Segundo && bandera2==true){uno->multijugador->MAbajo=true;uno->multijugador->moviendose=true;}
        }
    }
}
void MainWindow::keyReleaseEvent(QKeyEvent *evento){
    if(BloquearTeclado){
        if(evento->key()==Qt::Key_K){
            uno->jugador->MAbajo=false;
            uno->jugador->moviendose=false;
            uno->jugador->columnas=46.6;
            uno->jugador->update(-uno->jugador->ancho/2,-uno->jugador->alto/2,uno->jugador->ancho,uno->jugador->alto);
        }
        if(evento->key()==Qt::Key_L || evento->key()==Qt::Key_J)uno->jugador->moviendose=false;
        if(!uno->jugador->moviendose){
            uno->jugador->parabolicoderecho=false;
            uno->jugador->parabolicoizquierdo=false;
        }
        if(evento->key()==Qt::Key_S ){
            uno->multijugador->MAbajo=false;
            uno->multijugador->moviendose=false;
            uno->multijugador->columnas=46.6;
            uno->multijugador->update(-uno->multijugador->ancho/2,-uno->multijugador->alto/2,uno->multijugador->ancho,uno->multijugador->alto);
        }
        if(evento->key()==Qt::Key_D || evento->key()==Qt::Key_A)uno->multijugador->moviendose=false;
        if(!uno->multijugador->moviendose){
            uno->multijugador->parabolicoderecho=false;
            uno->multijugador->parabolicoizquierdo=false;
        }
        if(evento->key()==Qt::Key_L)uno->jugador->MDerecha=false;
        if(evento->key()==Qt::Key_J)uno->jugador->MIzquierda=false;
        if(evento->key()==Qt::Key_I){uno->jugador->MArriba=false;}
        if(evento->key()==Qt::Key_D && !uno->multijugador->Derecha)uno->multijugador->MDerecha=false;
        if(evento->key()==Qt::Key_A && !uno->multijugador->Izquierda)uno->multijugador->MIzquierda=false;
        if(evento->key()==Qt::Key_W )uno->multijugador->MArriba=false;

    }
}


// Destructor ) -----------------------------------------------------------------------------------
MainWindow::~MainWindow(){// Destructor
    delete ui;
    delete menu;
    delete partida;
    delete ranking;
    delete Bnueva;
    delete Bcargar;
    delete Branking;
    delete Aceptar;
    delete regresar;
    delete letrero;
    delete letrero2;
    delete letrero3;
    delete letrero4;
}
