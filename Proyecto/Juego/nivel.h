#ifndef NIVEL_H
#define NIVEL_H

#include"librerias.h"
#include "personajes.h"
#include"items.h"

class nivel:public QObject
{
    Q_OBJECT
private:
    QString nombre,nombre2; //nombre de jugador
    void keyPressEvent(QKeyEvent *);

    //hacer items y sus funciones
public:
    nivel(int p, int p2, int p3, int p4, Personajes*);
    nivel(int p, int p2, int p3, int p4, int p5, int p6, Personajes*, Personajes*);
    int vidas,vidas2;//vidas del jugador
    Personajes *jugador,*multijugador;
    QGraphicsLineItem *izquierda,*derecha,*abajo;
    QList<items*> obstaculos;
    QTimer *Obstaculos;
    QList<items*> bonificaciones;
    QTimer *Bonificaciones;
    QList<items*> plataformas;
    QTimer *Plataformas;
    QGraphicsScene *escenario;
    int friccion=2;
    QGraphicsLineItem *puerta;
    QLabel *Datos,*Datos2;
    int Px1,Py1,Px2,Py2;
    int Px3,Py3;

    QRectF boundingRect()const;
    void paint(QPainter *painter,const QStyleOptionGraphicsItem *style,QWidget *widget);
    ~nivel();
    void ActualizarItems();
    void darItems(items*,int);
    bool CheckPoint=false;
    void bonificar(items*);
    bool SonDos=false;


public slots:
    void Obstaculizando();
    void Plataformando();
    void Bonificacionando();
};

#endif // NIVEL_H
