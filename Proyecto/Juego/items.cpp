#include "items.h"
#include<QDebug>
items::items(int W, int H, int X, int Y,bool MOVIBLE,bool VERTICAL,bool _devuelve, QPixmap *painter,QObject *parent) : QObject(parent){
    ancho=W;
    alto=H;
    x=X;
    y=Y;
    inicial=x;
    final=644;
    Movibles=MOVIBLE;
    vertical=VERTICAL;
    devuelve=_devuelve;
    quieto=!devuelve;
    this->update(-ancho/2,-alto/2,ancho,alto);
    setPos(x,y);
    pintor=painter;

    MovimientoItem=new QTimer;
    MovimientoItem->start(5);
    connect(MovimientoItem,SIGNAL (timeout()),SLOT (MoverItem()));

}
void items::MoverItem(){//los que no devuelven.
    if(Movibles){
        if(vertical){
            if(!quieto){
                if(y<final && devuelve){y+=velocidad;setPos(x,y);}
                else if(y>=final){y-=velocidad;setPos(x,y);devuelve=false;}
                else if(y>inicial && !devuelve){y-=velocidad;setPos(x,y);}
                else devuelve=true;
            }else{
                 if(y<final){y+=velocidad;setPos(x,y);}
            }
        }else{
            if(x<final && devuelve ){x+=velocidad;setPos(x,y);}
            else if(x==final){x-=velocidad;setPos(x,y);devuelve=false;}
            else if(x>inicial && !devuelve){x-=velocidad;setPos(x,y);}
                 else devuelve=true;
        }
    }
}


QRectF items::boundingRect() const{return QRectF(-ancho/2,-alto/2,ancho,alto);}

void items::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *){
    painter->drawPixmap(-int(ancho/2),int(-alto/2),*pintor,int(columnas),int(filas),int(ancho),int(alto));
}
